SpaceX Cli RPG Developer Guide
---------------

SpaceX has been designed to be extendable and configurable.

The project is divided into two primary packages:
1. `com.anubhav.wrath.cli` :- This package contains code for rendering command line interface of the game.
2. `com.anubhav.wrath.core` :- This package contains the code for core Engine of the game.

Components
---
GameScreen
-----
Game screens are designed to provided consistent look-n-feel to the Player. A game screen describes an individual screen that Player sees. A game screen is generally divided into two components:
1. Header:- Top region
2. Body:- Bottom region containing content.

`com.anubhav.wrath.cli.screen.GameScreen` class has abstract implementation for a screen. Any implementation should extend this class.

See `com.anubhav.wrath.cli.screen.WelcomeScreen` for example.

Menu Based Screen
-----
A screen containing menu will be divided into three regions:
1. Header:- Top region
2. Body:- Middle region
3. Menu:- Bottom region with option to input menu selection.

A menu based screen can be implemented by extending `com.anubhav.wrath.cli.screen.MeniBasedScreen` class. The implementing class only needs to provide the id of menu to display by overriding getMenuId() method.

See `com.anubhav.wrath.cli.screen.StartNewGameScreen` for example.

Menu(s)
-----
Menus in the game are build using `com.anubhav.wrath.cli.menu.Menu` and `com.anubhav.wrath.cli.menu.MenuItem` classes. A Menu can contain a group of menu items. MenuItem is the selectable option provided to the Player.

Menus used in the application are configured programmatically using `com.anubhav.wrath.cli.config.MenuConfig` class and store in MenuStore cache.

MenuItemListener
-----
A MenuItemListener works as controller for MenuItem action. Each MenuItem should be associated with a MenuItemListener.

A menu item listener can be implemented by implementing `com.anubhav.wrath.cli.listener.MenuItemListener` interface.

See `com.anubhav.wrath.cli.listener.LoadGameMenuListener` for example.

Services
-----
Services are part of Core Engine of the game and are available in `com.anubhav.wrath.core.service` package. The services provide calculations and logic for running the game.

Model
-----
All the entities used in the system are available in `com.anubhav.wrath.core.model` package. 

`com.anubhav.wrath.core.model.AbstractDataObject` should be extended by all the entities defined in the system. This object is used in repositories for managing entities.

Entity Templates
-----
A template class is a simple entity class that defines the base configuration of an entity. 

For example:
A `com.anubhav.wrath.core.model.character.PlayerTemplate` class will contain all the default configurations for a new `com.anubhav.wrath.core.model.character.Player` like default weapons, health etc. When a new `com.anubhav.wrath.core.model.character.Player` class is instantiated it will be passed the `com.anubhav.wrath.core.model.character.PlayerTemplate` to use it as based configuration. 

Repositories
-----
A repository is used to store and retieve an entity from storage. Currently default in-memory implementation is provided for all repositories. This can be easily replaced by a JPA framework, like Spring-Repositories.

To implement a new repository, define an interface in `com.anubhav.wrath.core.repository` class and extend `com.anubhav.wrath.core.repository.CrudRepository` interface. An in-memory implementation is provided by implementing a class in `com.anubhav.wrath.core.repository.impl` package that also extends `com.anubhav.wrath.core.repository.impl.InMemoryRepositoryImpl` class. The `com.anubhav.wrath.core.repository.impl.InMemoryRepositoryImpl` class provides implementation of common methods.

See `com.anubhav.wrath.core.repository.GameSessionRepository` and `com.anubhav.wrath.core.repository.impl.GameSessionRepositoryImpl` for example.

BootstrapCore
-----
The `com.anubhav.wrath.core.BootstrapCore` class is invoked at the application start to load seed data.

Adding New Components
-------

Adding New Menu
1. Menu and MenuItems are configured during application start using `com.anubhav.wrath.cli.config.MenuConfig` class.
2. Create a MenuItemListener by extending the class `com.anubhav.wrath.cli.listener.MenuItemListener`.
3. Configure menu programmatically through `com.anubhav.wrath.cli.config.MenuConfig` class.


Adding New Screen
-----

1. Extend `com.anubhav.wrath.cli.screen.GameScreen` or `com.anubhav.wrath.cli.screen.MenuBasedScreen` to implement a new screen.


Adding New Core Data
-----

1. Weapons, Enemies, Player details are loaded as part of Core data.
2. Core data required for application is loaded using `com.anubhav.wrath.core.BootstrapCore` during application start.
3. Create and add new data object through `com.anubhav.wrath.core.BootstrapCore` class.

Change Display Text
-----

1. All display text is picked from `message.properties`.

Application Configurations
-----

1. Application configurations are loaded from `application.properties`.