# Welcome to SpaceX RPG game

## Game
The game is a command line based Role Playing Game. The game is based on space adventures, where the Player combats Aliens and wins weapons in return.

Note:- `This is an in-memory game, hence any stored values are lost on game restart. A persistence layer could be added for retaining values after restart. This feature has been left as part of todo list.`

## Compiling the project

This game is a Maven project, hence can be build by using standard maven commands.
```
mvn clean package
```

## Running the Game

From IDE
-----

Just run the `main` method in `com.anubhav.wrath.cli.GameLauncer` class

From Console
-----

Use below Maven command to start the game.
```
mvn exec:java
```

Note
-----
The project had been build and tested on Windows-10 system. But it should work fine on any other Java enabled platform.

[Developer Documentation](DEVELOPER.md)