package com.anubhav.wrath.core.config;

import org.junit.Assert;
import org.junit.Test;

public class ApplicationPropertiesTest {

  @Test
  public void testProperty() {
    Assert.assertEquals("test-player-temp-0001",
        ApplicationProperties.get(ApplicationPropertyKey.PLAYER_TEMPLATE_ID));
  }
}