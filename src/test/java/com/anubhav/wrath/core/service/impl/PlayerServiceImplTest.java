package com.anubhav.wrath.core.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.character.PlayerTemplate;
import com.anubhav.wrath.core.model.weapon.AttackWeapon;
import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.model.weapon.DefenseWeapon;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;
import com.anubhav.wrath.core.repository.PlayerTemplateRepository;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceImplTest {

  private static final String PLAYER_NAME = "Player Name";
  private static final String PLAYER_TEMPLATE_ID = "test-player-temp-0001";
  private static final String ATTACK_WEAPON_ID = "aw-0001";
  private static final String DEFENSE_WEAPON_ID = "dw-0001";
  private static final String INVALID_ATTACK_WEAPON_ID = "aw-invalid";
  private static final String INVALID_DEFENSE_WEAPON_ID = "dw-invalid";

  @Mock
  private PlayerTemplateRepository playerTemplateRepository;

  private PlayerServiceImpl playerService;

  @Before
  public void init() {
    playerService = new PlayerServiceImpl(playerTemplateRepository);
  }

  @Test
  public void createPlayer() {
    //Given
    PlayerTemplate playerTemplate = new PlayerTemplate();
    when(playerTemplateRepository.find(PLAYER_TEMPLATE_ID))
        .thenReturn(Optional.of(playerTemplate));

    //When
    Player player = playerService.create(PLAYER_NAME);

    //Then
    assertEquals(PLAYER_NAME, player.getName());
    assertEquals(playerTemplate, player.getTemplate());
  }

  @Test
  public void setValidAttackWeapon() {
    //Given
    Player player = createSamplePlayer();

    //When
    player = playerService.setActiveAttackWeapon(player, ATTACK_WEAPON_ID);

    //Then
    assertEquals(ATTACK_WEAPON_ID, player.getActiveAttackWeapon().getTemplateId());
  }

  @Test(expected = NotFoundException.class)
  public void setInvalidAttackWeapon() {
    //Given
    Player player = createSamplePlayer();

    //When
    playerService.setActiveAttackWeapon(player, INVALID_ATTACK_WEAPON_ID);
  }

  @Test
  public void setValidDefenseWeapon() {
    //Given
    Player player = createSamplePlayer();

    //When
    player = playerService.setActiveDefenseWeapon(player, DEFENSE_WEAPON_ID);

    //Then
    assertEquals(DEFENSE_WEAPON_ID, player.getActiveDefenseWeapon().getTemplateId());
  }

  @Test(expected = NotFoundException.class)
  public void setInvalidDefenseWeapon() {
    //Given
    Player player = createSamplePlayer();

    //When
    playerService.setActiveDefenseWeapon(player, INVALID_DEFENSE_WEAPON_ID);
  }

  private Player createSamplePlayer() {
    AttackWeaponTemplate attackWeaponTemplate = new AttackWeaponTemplate();
    attackWeaponTemplate.setId(ATTACK_WEAPON_ID);
    AttackWeapon attackWeapon = new AttackWeapon(attackWeaponTemplate);

    DefenseWeaponTemplate defenseWeaponTemplate = new DefenseWeaponTemplate();
    defenseWeaponTemplate.setId(DEFENSE_WEAPON_ID);
    DefenseWeapon defenseWeapon = new DefenseWeapon(defenseWeaponTemplate);

    Player player = new Player();
    player.addAttackWeapon(attackWeapon);
    player.addDefenseWeapon(defenseWeapon);
    return player;
  }

}