package com.anubhav.wrath.core.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.repository.GameSessionRepository;
import com.anubhav.wrath.core.service.GameSessionService;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameSessionServiceImplTest {

  @Mock
  private GameSessionRepository gameSessionRepository;

  private GameSessionService gameSessionService;

  @Before
  public void init() {
    gameSessionService = new GameSessionServiceImpl(gameSessionRepository);
  }

  @Test
  public void startNew() {
    //When
    GameSession gameSession = gameSessionService.startNew();

    //Then
    assertNotNull(gameSession);
  }

  @Test
  public void saveSession() {
    //Given
    GameSession request = new GameSession();

    //When
    gameSessionService.save(request);

    //Then
    Assert.assertNotNull(request.getName());
    Mockito.verify(gameSessionRepository).save(request);
  }

  @Test
  public void findAll() {
    //Given
    when(gameSessionRepository.findAll())
        .thenReturn(Arrays.asList(new GameSession(), new GameSession()));

    //When
    List<GameSession> result = gameSessionService.findAll();

    //Then
    Assert.assertEquals(2, result.size());
    Mockito.verify(gameSessionRepository).findAll();
  }
}