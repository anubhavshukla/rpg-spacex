package com.anubhav.wrath.core.service.impl;

import static com.anubhav.wrath.core.util.TestUtils.ENEMY_TEMPLATE_TWO;
import static com.anubhav.wrath.core.util.TestUtils.createEnemyTemplateOne;
import static com.anubhav.wrath.core.util.TestUtils.createEnemyTemplateTwo;
import static com.anubhav.wrath.core.util.TestUtils.createGameSession;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.core.calculator.DamageCalculator;
import com.anubhav.wrath.core.calculator.ExperiencePointCalculator;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.GameStatusEnum;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.Fighter;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.repository.EnemyTemplateRepository;
import com.anubhav.wrath.core.service.GameSessionService;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FightServiceImplTest {

  @Mock
  private ExperiencePointCalculator experiencePointCalculator;
  @Mock
  private DamageCalculator damageCalculator;
  @Mock
  private EnemyTemplateRepository enemyTemplateRepository;
  @Mock
  private GameSessionService gameSessionService;
  private FightServiceImpl fightService;

  @Before
  public void init() {
    fightService = new FightServiceImpl(damageCalculator, experiencePointCalculator,
        enemyTemplateRepository, gameSessionService);
  }

  @Test
  public void executeAttackNoResult() {
    //Given
    GameSession gameSession = createGameSession();
    when(damageCalculator.calculate(any(Fighter.class), any(Fighter.class))).thenReturn(10);

    //When
    GameSession result = fightService.executeAttack(gameSession);

    //Then
    assertEquals(FightStatusEnum.IN_PROGRESS, result.getFightStatus());
    assertEquals(90, result.getPlayer().getHealth());
    assertEquals(90, result.getActiveEnemy().getHealth());
  }

  @Test
  public void executeAttackPlayerLooses() {
    //Given
    GameSession gameSession = createGameSession();
    gameSession.getPlayer().setHealth(2);
    when(damageCalculator.calculate(any(Fighter.class), any(Fighter.class))).thenReturn(10);

    //When
    GameSession result = fightService.executeAttack(gameSession);

    //Then
    assertEquals(FightStatusEnum.LOST, result.getFightStatus());
    assertEquals(0, result.getPlayer().getHealth());
    assertEquals(90, result.getActiveEnemy().getHealth());
  }

  @Test
  public void executeAttackPlayerWins() {
    //Given
    GameSession gameSession = createGameSession();
    gameSession.getActiveEnemy().setHealth(5);
    when(damageCalculator.calculate(any(Fighter.class), any(Fighter.class))).thenReturn(10);
    when(enemyTemplateRepository.findBySelectionOrder(1)).thenReturn(Optional.empty());

    //When
    GameSession result = fightService.executeAttack(gameSession);

    //Then
    assertEquals(FightStatusEnum.WON, result.getFightStatus());
    assertEquals(100, result.getPlayer().getHealth());
    assertEquals(0, result.getActiveEnemy().getHealth());
  }

  @Test
  public void startFightNew() {
    //Given
    GameSession gameSession = createGameSession();
    gameSession.setActiveEnemy(null);
    gameSession.setFightStatus(FightStatusEnum.NOT_IN_PROGRESS);
    gameSession.setGameStatus(GameStatusEnum.IN_PROGRESS);
    gameSession.getPlayer().setActiveDefenseWeapon(null);
    gameSession.getPlayer().setActiveAttackWeapon(null);

    EnemyTemplate enemyTemplate = createEnemyTemplateOne();
    when(enemyTemplateRepository.findBySelectionOrder(0)).thenReturn(Optional.of(enemyTemplate));

    //When
    GameSession result = fightService.startFight(gameSession);

    //Then
    assertEquals(FightStatusEnum.IN_PROGRESS, result.getFightStatus());
    assertNotNull(result.getActiveEnemy());
  }

  @Test
  public void startFightNextEnemy() {
    //Given
    GameSession gameSession = createGameSession();
    gameSession.setFightStatus(FightStatusEnum.IN_PROGRESS);
    gameSession.setGameStatus(GameStatusEnum.IN_PROGRESS);

    EnemyTemplate enemyTemplate = createEnemyTemplateTwo();
    when(enemyTemplateRepository.findBySelectionOrder(1)).thenReturn(Optional.of(enemyTemplate));

    //When
    GameSession result = fightService.startFight(gameSession);

    //Then
    assertEquals(FightStatusEnum.IN_PROGRESS, result.getFightStatus());
    assertNotNull(result.getActiveEnemy());
    assertEquals(ENEMY_TEMPLATE_TWO, result.getActiveEnemy().getTemplate().getId());
  }
}