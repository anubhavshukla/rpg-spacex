package com.anubhav.wrath.core.di;

import com.anubhav.wrath.core.exception.DIException;
import com.anubhav.wrath.core.service.PlayerService;
import com.anubhav.wrath.core.service.impl.PlayerServiceImpl;
import org.junit.Assert;
import org.junit.Test;

public class BeanFactoryTest {

  @Test
  public void testValidBean() {
    Assert.assertTrue(BeanFactory.getBean(PlayerService.class) != null);
  }

  @Test(expected = DIException.class)
  public void testInvalidBean() {
    Assert.assertTrue(BeanFactory.getBean(PlayerServiceImpl.class) != null);
  }
}