package com.anubhav.wrath.core.util;

import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.character.PlayerTemplate;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.model.weapon.AttackWeapon;
import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.model.weapon.DefenseWeapon;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;

public class TestUtils {

  public static final String ENEMY_TEMPLATE_ONE = "0001";
  public static final String ENEMY_TEMPLATE_TWO = "0002";
  public static final String PLAYER_NAME = "PlayerName";

  public static GameSession createGameSession() {
    GameSession gameSession = new GameSession();

    AttackWeaponTemplate playerAttackWeaponTemp = new AttackWeaponTemplate();
    playerAttackWeaponTemp.setName("P-AW");
    playerAttackWeaponTemp.setMaxPower(20);
    playerAttackWeaponTemp.setMinPower(10);

    DefenseWeaponTemplate playerDefenseWeaponTemp = new DefenseWeaponTemplate();
    playerDefenseWeaponTemp.setName("P-DW");
    playerDefenseWeaponTemp.setMaxPower(5);
    playerDefenseWeaponTemp.setMinPower(3);

    PlayerTemplate playerTemplate = new PlayerTemplate();
    playerTemplate.setHealth(100);
    playerTemplate.setPrimaryAttackWeapon(playerAttackWeaponTemp);
    playerTemplate.setPrimaryDefenseWeapon(playerDefenseWeaponTemp);

    Player player = new Player(playerTemplate);
    player.setName(PLAYER_NAME);
    player.setExperience(60);
    player.setHealth(100);

    AttackWeapon playerAttackWeapon = new AttackWeapon(playerAttackWeaponTemp);
    player.setActiveAttackWeapon(playerAttackWeapon);

    DefenseWeapon defenseWeapon = new DefenseWeapon(playerDefenseWeaponTemp);
    player.setActiveDefenseWeapon(defenseWeapon);

    gameSession.setPlayer(player);

    EnemyTemplate enemyTemplate = createEnemyTemplate(ENEMY_TEMPLATE_ONE);

    Enemy activeEnemy = new Enemy(enemyTemplate);
    activeEnemy.setHealth(100);
    gameSession.setActiveEnemy(activeEnemy);
    gameSession.setFightStatus(FightStatusEnum.IN_PROGRESS);

    return gameSession;
  }

  public static EnemyTemplate createEnemyTemplate(String id) {
    AttackWeaponTemplate enemyAttackWeaponTemp = new AttackWeaponTemplate();
    enemyAttackWeaponTemp.setName("AW-" + id);
    enemyAttackWeaponTemp.setMaxPower(25);
    enemyAttackWeaponTemp.setMinPower(15);

    DefenseWeaponTemplate enemyDefenseWeaponTemp = new DefenseWeaponTemplate();
    enemyDefenseWeaponTemp.setName("DW-id");
    enemyDefenseWeaponTemp.setMaxPower(6);
    enemyDefenseWeaponTemp.setMinPower(5);

    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setId(id);
    enemyTemplate.setName(id);
    enemyTemplate.setSelectionOrder(0);
    enemyTemplate.setAttackWeapon(enemyAttackWeaponTemp);
    enemyTemplate.addAttackWeaponToEarn(enemyAttackWeaponTemp);
    enemyTemplate.setDefenseWeapon(enemyDefenseWeaponTemp);
    enemyTemplate.addDefenseWeaponToEarn(enemyDefenseWeaponTemp);
    return enemyTemplate;
  }

  public static EnemyTemplate createEnemyTemplateOne() {
    return createEnemyTemplate(ENEMY_TEMPLATE_ONE);
  }

  public static EnemyTemplate createEnemyTemplateTwo() {
    return createEnemyTemplate(ENEMY_TEMPLATE_TWO);
  }
}
