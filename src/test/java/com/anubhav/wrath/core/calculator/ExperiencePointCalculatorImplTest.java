package com.anubhav.wrath.core.calculator;

import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Assert;
import org.junit.Test;

public class ExperiencePointCalculatorImplTest {

  @Test
  public void calculate() {
    GameSession gameSession = new GameSession();

    Player player = new Player();
    player.setExperience(100);
    gameSession.setPlayer(player);

    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setExperiencePointsToEarn(50);
    Enemy enemy = new Enemy(enemyTemplate);
    gameSession.setActiveEnemy(enemy);

    ExperiencePointCalculatorImpl experiencePointCalculator = new ExperiencePointCalculatorImpl();
    Assert.assertEquals(150, experiencePointCalculator.calculate(gameSession));
  }

}