package com.anubhav.wrath.core.calculator;

import com.anubhav.wrath.core.model.character.Fighter;
import org.junit.Assert;
import org.junit.Test;

public class DamagePointCalculatorTest {

  @Test
  public void calculateDamage() {
    DamagePointCalculator damagePointCalculator = new DamagePointCalculator();
    Fighter fighter1 = getFighterOne();

    Fighter fighter2 = getFighterTwo();
    Assert.assertEquals(10, damagePointCalculator.calculate(fighter1, fighter2));
  }

  private Fighter getFighterTwo() {
    return new Fighter() {
      @Override
      public int getMaxAttackPoints() {
        return 15;
      }

      @Override
      public int getMinAttackPoints() {
        return 14;
      }

      @Override
      public int getMaxDefensePoints() {
        return 3;
      }

      @Override
      public int getMinDefensePoints() {
        return 2;
      }
    };
  }

  private Fighter getFighterOne() {
    return new Fighter() {
      @Override
      public int getMaxAttackPoints() {
        return 20;
      }

      @Override
      public int getMinAttackPoints() {
        return 19;
      }

      @Override
      public int getMaxDefensePoints() {
        return 5;
      }

      @Override
      public int getMinDefensePoints() {
        return 4;
      }
    };
  }

}