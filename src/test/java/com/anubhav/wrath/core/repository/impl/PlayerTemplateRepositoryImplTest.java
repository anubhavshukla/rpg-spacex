package com.anubhav.wrath.core.repository.impl;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.character.PlayerTemplate;
import com.anubhav.wrath.core.repository.PlayerTemplateRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PlayerTemplateRepositoryImplTest {

  private static final String SAMPLE_NAME = "Sample Name";
  private static final String UNSAVED_ID = "unsaved_id";

  private PlayerTemplateRepository repository;

  @Before
  public void init() {
    repository = new PlayerTemplateRepositoryImpl();
  }

  @Test
  public void saveTemplate() {
    PlayerTemplate playerTemplate = new PlayerTemplate();
    playerTemplate.setName(SAMPLE_NAME);

    PlayerTemplate savedTemplate = repository.save(playerTemplate);
    assertNotNull(savedTemplate.getId());
  }

  @Test
  public void findExistingValue() {
    PlayerTemplate playerTemplate = new PlayerTemplate();
    playerTemplate.setName(SAMPLE_NAME);

    PlayerTemplate savedTemplate = repository.save(playerTemplate);
    assertNotNull(repository.find(savedTemplate.getId()).orElseThrow(NotFoundException::new));
  }

  @Test(expected = NotFoundException.class)
  public void findNonExistingValue() {
    assertNotNull(repository.find(UNSAVED_ID).orElseThrow(NotFoundException::new));
  }

  @Test
  public void findAll() {
    repository.save(new PlayerTemplate());
    repository.save(new PlayerTemplate());

    Assert.assertEquals(2, repository.findAll().size());
  }
}