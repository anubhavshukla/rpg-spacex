package com.anubhav.wrath.core.repository.impl;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.repository.EnemyTemplateRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EnemyTemplateRepositoryImplTest {

  private static final String SAMPLE_NAME = "Sample Enemy";
  private static final String UNSAVED_ID = "unsaved_id";

  private EnemyTemplateRepository repository;

  @Before
  public void init() {
    repository = new EnemyTemplateRepositoryImpl();
  }

  @Test
  public void saveTemplate() {
    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setName(SAMPLE_NAME);

    EnemyTemplate savedTemplate = repository.save(enemyTemplate);
    assertNotNull(savedTemplate.getId());
  }

  @Test
  public void findExistingValue() {
    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setName(SAMPLE_NAME);

    EnemyTemplate savedTemplate = repository.save(enemyTemplate);
    assertNotNull(repository.find(savedTemplate.getId()).orElseThrow(NotFoundException::new));
  }

  @Test(expected = NotFoundException.class)
  public void findNonExistingValue() {
    assertNotNull(repository.find(UNSAVED_ID).orElseThrow(NotFoundException::new));
  }

  @Test
  public void findAll() {
    repository.save(new EnemyTemplate());
    repository.save(new EnemyTemplate());

    Assert.assertEquals(2, repository.findAll().size());
  }
}