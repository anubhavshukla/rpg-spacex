package com.anubhav.wrath.core.repository.impl;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.repository.AttackWeaponTemplateRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AttackWeaponTemplateRepositoryImplTest {

  private AttackWeaponTemplateRepository repository;

  @Before
  public void init() {
    repository = new AttackWeaponTemplateRepositoryImpl();
  }

  @Test
  public void saveTemplate() {
    AttackWeaponTemplate attackWeapon = new AttackWeaponTemplate();
    attackWeapon.setName("Sample Attack Weapon");

    AttackWeaponTemplate savedTemplate = repository.save(attackWeapon);
    assertNotNull(savedTemplate.getId());
  }

  @Test
  public void findExistingValue() {
    AttackWeaponTemplate attackWeapon = new AttackWeaponTemplate();
    attackWeapon.setName("Sample Attack Weapon");

    AttackWeaponTemplate savedTemplate = repository.save(attackWeapon);
    assertNotNull(repository.find(savedTemplate.getId()).orElseThrow(NotFoundException::new));
  }

  @Test(expected = NotFoundException.class)
  public void findNonExistingValue() {
    assertNotNull(repository.find("unsaved_id").orElseThrow(NotFoundException::new));
  }

  @Test
  public void findAll() {
    repository.save(new AttackWeaponTemplate());
    repository.save(new AttackWeaponTemplate());

    Assert.assertEquals(2, repository.findAll().size());
  }
}