package com.anubhav.wrath.core.repository.impl;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;
import com.anubhav.wrath.core.repository.DefenseWeaponTemplateRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DefenseWeaponTemplateRepositoryImplTest {

  private DefenseWeaponTemplateRepository repository;

  @Before
  public void init() {
    repository = new DefenseWeaponTemplateRepositoryImpl();
  }

  @Test
  public void saveTemplate() {
    DefenseWeaponTemplate defenseWeaponTemplate = new DefenseWeaponTemplate();
    defenseWeaponTemplate.setName("Sample Defense Weapon");

    DefenseWeaponTemplate savedTemplate = repository.save(defenseWeaponTemplate);
    assertNotNull(savedTemplate.getId());
  }

  @Test
  public void findExistingValue() {
    DefenseWeaponTemplate defenseWeaponTemplate = new DefenseWeaponTemplate();
    defenseWeaponTemplate.setName("Sample Defense Weapon");

    DefenseWeaponTemplate savedTemplate = repository.save(defenseWeaponTemplate);
    assertNotNull(repository.find(savedTemplate.getId()).orElseThrow(NotFoundException::new));
  }

  @Test(expected = NotFoundException.class)
  public void findNonExistingValue() {
    assertNotNull(repository.find("unsaved_id").orElseThrow(NotFoundException::new));
  }

  @Test
  public void findAll() {
    repository.save(new DefenseWeaponTemplate());
    repository.save(new DefenseWeaponTemplate());

    Assert.assertEquals(2, repository.findAll().size());
  }

}