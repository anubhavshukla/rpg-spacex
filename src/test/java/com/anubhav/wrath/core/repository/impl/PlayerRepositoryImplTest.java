package com.anubhav.wrath.core.repository.impl;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.repository.PlayerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PlayerRepositoryImplTest {

  private static final String SAMPLE_NAME = "Sample Name";
  private static final String UNSAVED_ID = "unsaved_id";

  private PlayerRepository repository;

  @Before
  public void init() {
    repository = new PlayerRepositoryImpl();
  }

  @Test
  public void saveTemplate() {
    Player player = new Player();
    player.setName(SAMPLE_NAME);

    Player savedTemplate = repository.save(player);
    assertNotNull(savedTemplate.getId());
  }

  @Test
  public void findExistingValue() {
    Player player = new Player();
    player.setName(SAMPLE_NAME);

    Player savedTemplate = repository.save(player);
    assertNotNull(repository.find(savedTemplate.getId()).orElseThrow(NotFoundException::new));
  }

  @Test(expected = NotFoundException.class)
  public void findNonExistingValue() {
    assertNotNull(repository.find(UNSAVED_ID).orElseThrow(NotFoundException::new));
  }

  @Test
  public void findAll() {
    repository.save(new Player());
    repository.save(new Player());

    Assert.assertEquals(2, repository.findAll().size());
  }
}