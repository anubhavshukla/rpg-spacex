package com.anubhav.wrath.core.repository.impl;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.repository.GameSessionRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameSessionRepositoryImplTest {

  private static final String SAMPLE_NAME = "Sample Name";
  private static final String UNSAVED_ID = "unsaved_id";

  private GameSessionRepository repository;

  @Before
  public void init() {
    repository = new GameSessionRepositoryImpl();
  }

  @Test
  public void saveTemplate() {
    GameSession gameSession = new GameSession();
    gameSession.setName(SAMPLE_NAME);

    GameSession savedTemplate = repository.save(gameSession);
    assertNotNull(savedTemplate.getId());
  }

  @Test
  public void findExistingValue() {
    GameSession gameSession = new GameSession();
    gameSession.setName(SAMPLE_NAME);

    GameSession savedTemplate = repository.save(gameSession);
    assertNotNull(repository.find(savedTemplate.getId()).orElseThrow(NotFoundException::new));
  }

  @Test(expected = NotFoundException.class)
  public void findNonExistingValue() {
    assertNotNull(repository.find(UNSAVED_ID).orElseThrow(NotFoundException::new));
  }

  @Test
  public void findAll() {
    repository.save(new GameSession());
    repository.save(new GameSession());

    Assert.assertEquals(2, repository.findAll().size());
  }
}