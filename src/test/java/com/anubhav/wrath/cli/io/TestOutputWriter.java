package com.anubhav.wrath.cli.io;

import com.anubhav.wrath.core.i18n.MessageResource;

public class TestOutputWriter implements OutputWriter {

  private StringBuilder aggregator = new StringBuilder();

  @Override
  public void write(String message) {
    aggregator.append(message);
  }

  @Override
  public void separator() {
    aggregator.append(MessageResource.getMessage("screen.separator"));
  }

  public String getFullText() {
    return aggregator.toString();
  }
}
