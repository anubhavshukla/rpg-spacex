package com.anubhav.wrath.cli.listener;

import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.screen.WelcomeScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.GameSessionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SaveSessionMenuListenerTest {

  @Mock
  private GameSessionService gameSessionService;

  @Mock
  private WelcomeScreen welcomeScreen;

  private SaveSessionMenuListener listener;

  @Before
  public void init() {
    listener = new SaveSessionMenuListener(gameSessionService, welcomeScreen);
  }

  @Test
  public void executeListener() {
    //Given
    GameSession gameSession = new GameSession();

    //When
    listener.execute(gameSession);

    //Then
    verify(gameSessionService).save(gameSession);
    verify(welcomeScreen).render(null);
  }

}