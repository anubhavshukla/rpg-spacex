package com.anubhav.wrath.cli.listener;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.cli.screen.FightInprogressScreen;
import com.anubhav.wrath.cli.screen.FightLostScreen;
import com.anubhav.wrath.cli.screen.FightWonScreen;
import com.anubhav.wrath.cli.screen.GameWonScreen;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.GameStatusEnum;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.FightService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StrikeEnemyMenuListenerTest {

  @Mock
  private FightService fightService;

  @Mock
  private GameWonScreen gameWonScreen;

  @Mock
  private FightWonScreen fightWonScreen;

  @Mock
  private FightLostScreen fightLostScreen;

  @Mock
  private FightInprogressScreen fightInprogressScreen;

  private StrikeEnemyMenuListener listener;

  @Before
  public void init() {
    listener = new StrikeEnemyMenuListener(fightService, gameWonScreen, fightWonScreen,
        fightLostScreen, fightInprogressScreen);
  }

  @Test
  public void executeWithFightInProgressResult() {
    //Given
    GameSession requestSession = new GameSession();
    GameSession responseSession = new GameSession();
    responseSession.setFightStatus(FightStatusEnum.IN_PROGRESS);
    when(fightService.executeAttack(requestSession)).thenReturn(responseSession);

    //When
    listener.execute(requestSession);

    //Then
    verify(fightService).executeAttack(requestSession);
    verify(fightInprogressScreen).render(responseSession);
  }

  @Test
  public void executeWithFightLostResult() {
    //Given
    GameSession requestSession = new GameSession();
    GameSession responseSession = new GameSession();
    responseSession.setFightStatus(FightStatusEnum.LOST);
    when(fightService.executeAttack(requestSession)).thenReturn(responseSession);

    //When
    listener.execute(requestSession);

    //Then
    verify(fightService).executeAttack(requestSession);
    verify(fightLostScreen).render(responseSession);
  }

  @Test
  public void executeWithFightWonResult() {
    //Given
    GameSession requestSession = new GameSession();
    GameSession responseSession = new GameSession();
    responseSession.setFightStatus(FightStatusEnum.WON);
    when(fightService.executeAttack(requestSession)).thenReturn(responseSession);

    //When
    listener.execute(requestSession);

    //Then
    verify(fightService).executeAttack(requestSession);
    verify(fightWonScreen).render(responseSession);
  }

  @Test
  public void executeWithGameCompleteResult() {
    //Given
    GameSession requestSession = new GameSession();
    GameSession responseSession = new GameSession();
    responseSession.setGameStatus(GameStatusEnum.COMPLETED);
    when(fightService.executeAttack(requestSession)).thenReturn(responseSession);

    //When
    listener.execute(requestSession);

    //Then
    verify(fightService).executeAttack(requestSession);
    verify(gameWonScreen).render(responseSession);
  }
}