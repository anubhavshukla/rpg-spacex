package com.anubhav.wrath.cli.listener;

import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.screen.GameRulesScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameRulesListenerTest {

  @Mock
  private GameRulesScreen gameRulesScreen;

  private GameRulesListener listener;

  @Before
  public void init() {
    listener = new GameRulesListener(gameRulesScreen);
  }

  @Test
  public void executeListener() {
    //Given
    GameSession gameSession = new GameSession();

    //When
    listener.execute(gameSession);

    //Then
    verify(gameRulesScreen).render(gameSession);
  }

}