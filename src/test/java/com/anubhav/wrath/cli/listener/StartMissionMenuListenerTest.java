package com.anubhav.wrath.cli.listener;

import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.screen.FightInprogressScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.FightService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StartMissionMenuListenerTest {

  @Mock
  private FightService fightService;

  @Mock
  private FightInprogressScreen fightInprogressScreen;

  private StartMissionMenuListener listener;

  @Before
  public void init() {
    listener = new StartMissionMenuListener(fightService, fightInprogressScreen);
  }

  @Test
  public void executeListener() {
    //Given
    GameSession gameSession = new GameSession();

    //When
    listener.execute(gameSession);

    //Then
    verify(fightService).startFight(gameSession);
    verify(fightInprogressScreen).render(gameSession);
  }
}