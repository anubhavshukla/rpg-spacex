package com.anubhav.wrath.cli.listener;

import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.screen.ChangeDefenseWeaponScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ChangeDefenseWeaponMenuListenerTest {

  @Mock
  private ChangeDefenseWeaponScreen changeDefenseWeaponScreen;

  private ChangeDefenseWeaponMenuListener listener;

  @Before
  public void init() {
    listener = new ChangeDefenseWeaponMenuListener(changeDefenseWeaponScreen);
  }

  @Test
  public void executeListener() {
    //Given
    GameSession session = new GameSession();

    //When
    listener.execute(session);

    //Then
    verify(changeDefenseWeaponScreen).render(session);
  }
}