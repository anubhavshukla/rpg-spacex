package com.anubhav.wrath.cli.listener;

import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.screen.WelcomeScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BackToMainMenuListenerTest {

  @Mock
  private WelcomeScreen welcomeScreen;

  private BackToMainMenuListener listener;

  @Before
  public void init() {
    listener = new BackToMainMenuListener(welcomeScreen);
  }

  @Test
  public void executeListener() {
    //Given
    GameSession gameSession = new GameSession();

    //When
    listener.execute(gameSession);

    //Then
    verify(welcomeScreen).render(gameSession);
  }
}