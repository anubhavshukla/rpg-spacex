package com.anubhav.wrath.cli.listener;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.screen.CreatePlayerScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.GameSessionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NewGameMenuListenerTest {

  @Mock
  private GameSessionService gameSessionService;

  @Mock
  private CreatePlayerScreen createPlayerScreen;

  private NewGameMenuListener listener;

  @Before
  public void inti() {
    listener = new NewGameMenuListener(gameSessionService, createPlayerScreen);
  }

  @Test
  public void executeListener() {
    //When
    listener.execute(null);

    //Then
    verify(gameSessionService).startNew();
    verify(createPlayerScreen).render(any(GameSession.class));
  }
}