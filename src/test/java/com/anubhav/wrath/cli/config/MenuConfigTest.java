package com.anubhav.wrath.cli.config;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.cli.menu.Menu;
import com.anubhav.wrath.cli.store.MenuStore;
import com.anubhav.wrath.core.exception.NotFoundException;
import org.junit.Test;

public class MenuConfigTest {

  @Test
  public void configureMenus() {
    //Given
    new MenuConfig().configure();

    //When
    Menu mainMenu = MenuStore.get("main-menu").orElseThrow(NotFoundException::new);

    //Then
    assertNotNull(mainMenu);
    assertNotNull(mainMenu.getMenuItems());
    assertNotNull(mainMenu.getId());
    assertNotNull(mainMenu.getGreeting());
  }
}