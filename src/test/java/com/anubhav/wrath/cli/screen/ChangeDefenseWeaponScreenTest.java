package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.model.weapon.DefenseWeapon;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;
import com.anubhav.wrath.core.service.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ChangeDefenseWeaponScreenTest {

  private static final String DEFENSE_WEAPON_TWO = "dw-0002";
  private static final String DEFENSE_WEAPON_ONE = "dw-0001";

  @Mock
  private PlayerService playerService;
  @Mock
  private FightInprogressScreen fightInprogressScreen;
  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;

  private ChangeDefenseWeaponScreen changeDefenseWeaponScreen;

  @Before
  public void init() {
    changeDefenseWeaponScreen = new ChangeDefenseWeaponScreen(outputWriter, inputReader,
        playerService, fightInprogressScreen);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.changedefenseweapon.header"),
        changeDefenseWeaponScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBody() {
    //Given
    GameSession gameSession = new GameSession();

    Player player = new Player();
    DefenseWeaponTemplate defenseWeaponTemp = new DefenseWeaponTemplate();
    defenseWeaponTemp.setId(DEFENSE_WEAPON_ONE);
    DefenseWeapon activeDefenseWeapon = new DefenseWeapon(defenseWeaponTemp);
    player.addDefenseWeapon(activeDefenseWeapon);
    player.setActiveDefenseWeapon(activeDefenseWeapon);

    DefenseWeaponTemplate defenseWeaponTemplate = new DefenseWeaponTemplate();
    defenseWeaponTemplate.setId(DEFENSE_WEAPON_TWO);
    DefenseWeapon defenseWeapon = new DefenseWeapon(defenseWeaponTemplate);
    player.addDefenseWeapon(defenseWeapon);
    gameSession.setPlayer(player);

    Player newPlayer = new Player();
    newPlayer.setActiveDefenseWeapon(defenseWeapon);

    when(inputReader.read()).thenReturn("2");
    when(playerService.setActiveDefenseWeapon(player, DEFENSE_WEAPON_TWO))
        .thenReturn(newPlayer);

    //When
    changeDefenseWeaponScreen.outputBody(gameSession);

    //Then
    verify(fightInprogressScreen).render(gameSession);
    verify(playerService).setActiveDefenseWeapon(player, DEFENSE_WEAPON_TWO);
    assertEquals(DEFENSE_WEAPON_TWO,
        gameSession.getPlayer().getActiveDefenseWeapon().getTemplateId());
  }

}