package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CreatePlayerScreenTest {

  private static final String SAMPLE_NAME = "Sample Name";

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  @Mock
  private PlayerService playerService;
  @Mock
  private StartNewGameScreen startNewGameScreen;

  private CreatePlayerScreen createPlayerScreen;

  @Before
  public void init() {
    createPlayerScreen = new CreatePlayerScreen(outputWriter, inputReader, playerService,
        startNewGameScreen);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.createplayer.header"),
        createPlayerScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBody() {
    //Given
    GameSession gameSession = new GameSession();
    Player newPlayer = new Player();
    newPlayer.setName(SAMPLE_NAME);

    when(playerService.create(SAMPLE_NAME)).thenReturn(newPlayer);
    when(inputReader.read()).thenReturn(SAMPLE_NAME);

    //When
    createPlayerScreen.outputBody(gameSession);

    //Then
    verify(startNewGameScreen).render(gameSession);
    verify(playerService).create(SAMPLE_NAME);
    assertEquals(SAMPLE_NAME, gameSession.getPlayer().getName());
  }
}