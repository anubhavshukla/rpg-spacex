package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.model.weapon.AttackWeapon;
import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.service.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ChangeAttackWeaponScreenTest {

  private static final String ATTACK_WEAPON_TWO = "aw-0002";
  private static final String ATTACK_WEAPON_ONE = "aw-0001";

  @Mock
  private PlayerService playerService;
  @Mock
  private FightInprogressScreen fightInprogressScreen;
  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;

  private ChangeAttackWeaponScreen changeAttackWeaponScreen;

  @Before
  public void init() {
    changeAttackWeaponScreen = new ChangeAttackWeaponScreen(outputWriter, inputReader,
        playerService, fightInprogressScreen);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.changeattackweapon.header"),
        changeAttackWeaponScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBody() {
    //Given
    GameSession gameSession = new GameSession();
    Player player = new Player();
    AttackWeaponTemplate activeAttackWeaponTemp = new AttackWeaponTemplate();
    activeAttackWeaponTemp.setId(ATTACK_WEAPON_ONE);
    AttackWeapon activeAttackWeapon = new AttackWeapon(activeAttackWeaponTemp);
    player.addAttackWeapon(activeAttackWeapon);
    player.setActiveAttackWeapon(activeAttackWeapon);
    AttackWeaponTemplate attackWeaponTemplate = new AttackWeaponTemplate();
    attackWeaponTemplate.setId(ATTACK_WEAPON_TWO);
    AttackWeapon attackWeapon = new AttackWeapon(attackWeaponTemplate);
    player.addAttackWeapon(attackWeapon);
    gameSession.setPlayer(player);

    Player newPlayer = new Player();
    newPlayer.setActiveAttackWeapon(attackWeapon);

    when(inputReader.read()).thenReturn("2");
    when(playerService.setActiveAttackWeapon(player, ATTACK_WEAPON_TWO))
        .thenReturn(newPlayer);

    //When
    changeAttackWeaponScreen.outputBody(gameSession);

    //Then
    verify(fightInprogressScreen).render(gameSession);
    verify(playerService).setActiveAttackWeapon(player, ATTACK_WEAPON_TWO);
    assertEquals(ATTACK_WEAPON_TWO,
        gameSession.getPlayer().getActiveAttackWeapon().getTemplateId());
  }

}