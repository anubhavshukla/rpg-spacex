package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.exception.InvalidStateException;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FightLostScreenTest {

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  @Mock
  private WelcomeScreen welcomeScreen;
  private FightLostScreen fightLostScreen;

  @Before
  public void init() {
    fightLostScreen = new FightLostScreen(outputWriter, inputReader, welcomeScreen);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.fightlost.header"),
        fightLostScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBodySuccess() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setPlayer(new Player());
    gameSession.setFightStatus(FightStatusEnum.LOST);

    //When
    fightLostScreen.outputBody(gameSession);

    //Then
    verify(inputReader).read();
    verify(welcomeScreen).render(gameSession);
  }

  @Test(expected = InvalidStateException.class)
  public void verifyOutputBodyForInvalidGameSession() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setFightStatus(FightStatusEnum.LOST);

    //When
    fightLostScreen.outputBody(gameSession);
  }

  @Test(expected = InvalidStateException.class)
  public void verifyOutputBodyForInvalidFightState() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setPlayer(new Player());

    //When
    fightLostScreen.outputBody(gameSession);
  }

}