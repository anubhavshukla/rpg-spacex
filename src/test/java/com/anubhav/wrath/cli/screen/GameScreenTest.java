package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.TestOutputWriter;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameScreenTest {

  private static final String PLAYER_NAME = "Sample Name";

  @Mock
  private InputReader inputReader;
  private TestOutputWriter outputWriter;
  private GameScreen gameScreen;

  @Before
  public void init() {
    outputWriter = new TestOutputWriter();
    gameScreen = getGameScreen();
  }

  @Test
  public void write() {
    //Given
    String message = "Hello! Test message.";

    //When
    gameScreen.write(message);

    //Then
    assertEquals(message, outputWriter.getFullText());
  }

  @Test
  public void writeMessage() {
    //Given
    String messageKey = "test.gamescreen.sample";

    //When
    gameScreen.writeMessage(messageKey);

    //Then
    assertEquals(getMessage(messageKey), outputWriter.getFullText());
  }

  @Test
  public void writeMessageWithArguments() {
    //Given
    String messageKey = "test.gamescreen.sample.args";

    //When
    gameScreen.writeMessage(messageKey, "name");

    //Then
    assertEquals(getMessage(messageKey, "name"), outputWriter.getFullText());
  }

  @Test
  public void outputSeparator() {
    //When
    gameScreen.outputSeparator();

    //Then
    assertEquals(getMessage("screen.separator"), outputWriter.getFullText());
  }

  @Test
  public void outputHeaderForGuest() {
    //When
    gameScreen.outputHeader(null);

    //Then
    assertEquals(getMessage("test.gamescreen.header.guest"), outputWriter.getFullText());
  }

  @Test
  public void outputHeaderForPlayer() {
    //Given
    GameSession gameSession = new GameSession();
    Player player = new Player();
    player.setName(PLAYER_NAME);
    gameSession.setPlayer(player);

    //When
    gameScreen.outputHeader(gameSession);

    //Then
    assertEquals(getMessage("test.gamescreen.header.player"),
        outputWriter.getFullText());
  }

  @Test
  public void outputPlayerStats() {
    //Given
    GameSession gameSession = TestUtils.createGameSession();

    //When
    gameScreen.outputPlayerStats(gameSession.getPlayer());

    //Then
    assertEquals(getMessage("test.gamescreen.player.stats"),
        outputWriter.getFullText());
  }

  private GameScreen getGameScreen() {
    return new GameScreen(outputWriter, inputReader) {
      @Override
      protected void outputBody(GameSession session) {
      }

      @Override
      protected String getHeaderMessage() {
        return "Test Screen";
      }
    };
  }

}