package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.exception.InvalidStateException;
import com.anubhav.wrath.core.model.GameStatusEnum;
import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameWonScreenTest {

  private static final String ENEMY_NAME = "Enemy Name";

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  @Mock
  private WelcomeScreen welcomeScreen;
  private GameWonScreen gameWonScreen;

  @Before
  public void init() {
    gameWonScreen = spy(new GameWonScreen(outputWriter, inputReader, welcomeScreen));
    doNothing().when(gameWonScreen).outputPlayerStats(any(Player.class));
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.gamewon.header"),
        gameWonScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBodySuccess() {
    //Given
    GameSession gameSession = new GameSession();

    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setName(ENEMY_NAME);
    Enemy enemy = new Enemy(enemyTemplate);
    gameSession.setActiveEnemy(enemy);

    gameSession.setPlayer(new Player());
    gameSession.setGameStatus(GameStatusEnum.COMPLETED);

    //When
    gameWonScreen.outputBody(gameSession);

    //Then
    verify(welcomeScreen).render(null);
  }

  @Test(expected = InvalidStateException.class)
  public void verifyOutputBodyForInvalidGameSession() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setGameStatus(GameStatusEnum.COMPLETED);

    //When
    gameWonScreen.outputBody(gameSession);
  }

  @Test(expected = InvalidStateException.class)
  public void verifyOutputBodyForInvalidFightState() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setPlayer(new Player());

    //When
    gameWonScreen.outputBody(gameSession);
  }
}