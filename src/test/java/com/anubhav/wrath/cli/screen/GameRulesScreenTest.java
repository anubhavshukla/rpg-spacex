package com.anubhav.wrath.cli.screen;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameRulesScreenTest {

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  @Mock
  private WelcomeScreen welcomeScreen;
  private GameRulesScreen gameRulesScreen;

  @Before
  public void init() {
    gameRulesScreen = new GameRulesScreen(outputWriter, inputReader, welcomeScreen);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(MessageResource.getMessage("screen.gamerules.header"),
        gameRulesScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBodyWithoutCurrentScreen() {
    //Given
    GameSession gameSession = new GameSession();

    //When
    gameRulesScreen.outputBody(gameSession);

    //Then
    verify(welcomeScreen).render(gameSession);
  }
}