package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.TestOutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FightInprogressScreenTest {

  private TestOutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  private FightInprogressScreen fightInprogressScreen;

  @Before
  public void init() {
    outputWriter = new TestOutputWriter();
    fightInprogressScreen = new FightInprogressScreen(outputWriter, inputReader);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.fightinprogress.header"),
        fightInprogressScreen.getHeaderMessage());
  }

  @Test
  public void verifyMenuId() {
    assertEquals("fight-menu", fightInprogressScreen.getMenuId());
  }

  @Test
  public void outputBody() {
    //Given
    GameSession gameSession = TestUtils.createGameSession();

    //When
    fightInprogressScreen.outputBody(gameSession);

    //Then
    assertEquals(MessageResource.getMessage("test.fightinprogressscreen.output"),
        outputWriter.getFullText());
  }
}