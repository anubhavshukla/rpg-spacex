package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.GameSessionService;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LoadSessionScreenTest {

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  @Mock
  private GameSessionService gameSessionService;
  @Mock
  private WelcomeScreen welcomeScreen;
  private LoadSessionScreen loadSessionScreen;

  @Before
  public void init() {
    loadSessionScreen = new LoadSessionScreen(outputWriter, inputReader, gameSessionService,
        welcomeScreen);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.loadsession.header"),
        loadSessionScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBodyNoSession() {
    //Given
    GameSession gameSession = new GameSession();

    when(gameSessionService.findAll()).thenReturn(Collections.emptyList());

    //When
    loadSessionScreen.outputBody(gameSession);

    //Then
    verify(welcomeScreen).render(gameSession);
  }

  @Test
  public void verifyOutputBodyWithSessionAndNoChange() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setName("Saved Session 0001");
    Player player = new Player();
    player.setName("Player Name");
    gameSession.setPlayer(player);

    when(gameSessionService.findAll()).thenReturn(Collections.singletonList(gameSession));
    when(inputReader.read()).thenReturn("0");

    //When
    loadSessionScreen.outputBody(gameSession);

    //Then
    verify(welcomeScreen).render(gameSession);
  }
}