package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExitGameScreenTest {

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  private ExitGameScreen exitGameScreen;

  @Before
  public void init() {
    exitGameScreen = new ExitGameScreen(outputWriter, inputReader);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.exitgame.header"),
        exitGameScreen.getHeaderMessage());
  }

  @Test
  public void verifyOutputBody() {
    //Given
    GameSession gameSession = new GameSession();

    //When
    exitGameScreen.outputBody(gameSession);

    //Then
    verify(inputReader).close();
  }
}