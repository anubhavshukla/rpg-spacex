package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.anubhav.wrath.cli.config.MenuConfig;
import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.TestOutputWriter;
import com.anubhav.wrath.cli.menu.MenuItem;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MenuBasedScreenTest {

  @Mock
  private TestOutputWriter outputWriter;
  @Mock
  private InputReader inputReader;

  private MenuBasedScreen menuBasedScreen;

  @Before
  public void init() {
    outputWriter = new TestOutputWriter();
    menuBasedScreen = getMenuBasedScreen();
    new MenuConfig().configure();
  }

  @Test
  public void readMenuInput() {
    //Given
    when(inputReader.read()).thenReturn("x", "1");

    //When
    String result = menuBasedScreen.readMenuInput();

    //Then
    assertEquals("1", result);
  }

  @Test
  public void outputMenu() {
    //When
    menuBasedScreen.outputMenu();

    //Then
    assertEquals(getMessage("test.menubased.output.menu"), outputWriter.getFullText());
  }

  @Test
  public void findMenuItem() {
    //When
    MenuItem menuItem = menuBasedScreen.findMenuItem("1");

    //Then
    assertEquals("1", menuItem.getShortCode());
  }

  private MenuBasedScreen getMenuBasedScreen() {
    return new MenuBasedScreen(outputWriter, inputReader) {
      @Override
      protected String getMenuId() {
        return "main-menu";
      }

      @Override
      protected void outputBody(GameSession session) {

      }

      @Override
      protected String getHeaderMessage() {
        return null;
      }
    };
  }
}