package com.anubhav.wrath.cli.screen;


import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.exception.InvalidStateException;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FightWonScreenTest {

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;
  private FightWonScreen fightWonScreen;

  @Before
  public void init() {
    fightWonScreen = spy(new FightWonScreen(outputWriter, inputReader));
    doNothing().when(fightWonScreen).outputPlayerStats(any(Player.class));
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.fightwon.header"),
        fightWonScreen.getHeaderMessage());
  }

  @Test
  public void verifyMenuId() {
    assertEquals("fight-won-menu",
        fightWonScreen.getMenuId());
  }

  @Test
  public void verifyOutputBodySuccess() {
    //Given
    GameSession gameSession = new GameSession();

    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setName("Enemy Name");
    Enemy enemy = new Enemy(enemyTemplate);
    gameSession.setActiveEnemy(enemy);

    gameSession.setPlayer(new Player());
    gameSession.setFightStatus(FightStatusEnum.WON);

    //When
    fightWonScreen.outputBody(gameSession);
  }

  @Test(expected = InvalidStateException.class)
  public void verifyOutputBodyForInvalidGameSession() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setFightStatus(FightStatusEnum.WON);

    //When
    fightWonScreen.outputBody(gameSession);
  }

  @Test(expected = InvalidStateException.class)
  public void verifyOutputBodyForInvalidFightState() {
    //Given
    GameSession gameSession = new GameSession();
    gameSession.setPlayer(new Player());

    //When
    fightWonScreen.outputBody(gameSession);
  }
}