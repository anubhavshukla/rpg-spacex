package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static org.junit.Assert.assertEquals;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StartNewGameScreenTest {

  @Mock
  private OutputWriter outputWriter;
  @Mock
  private InputReader inputReader;

  private StartNewGameScreen startNewGameScreen;

  @Before
  public void init() {
    startNewGameScreen = new StartNewGameScreen(outputWriter, inputReader);
  }

  @Test
  public void verifyHeaderMessage() {
    assertEquals(getMessage("screen.startnewgame.header"),
        startNewGameScreen.getHeaderMessage());
  }

  @Test
  public void verifyMenuId() {
    assertEquals("player-menu", startNewGameScreen.getMenuId());
  }
}