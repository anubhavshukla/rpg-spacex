package com.anubhav.wrath.cli.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {

  @Test
  public void appendAtLocationTwoStrings() {
    String result = StringUtils.appendAtLocation("aa", "bb", 3);
    Assert.assertEquals("aa bb", result);
  }

  @Test
  public void appendAtLoationMultipleStrings() {
    String result = StringUtils.appendAtLocation(new String[]{"aa", "bb", "cc"}, 3);
    Assert.assertEquals("aa bb cc", result);
  }

  @Test
  public void showInColumns() {
    String result = StringUtils.showInColumns("aa", "bb", "cc");
    Assert.assertEquals("aa  bb  cc", result);
  }
}