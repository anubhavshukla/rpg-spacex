package com.anubhav.wrath.cli.utils;

import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.character.PlayerTemplate;
import org.junit.Assert;
import org.junit.Test;

public class DisplayUtilsTest {

  @Test
  public void formattedHealthForPlayer() {
    //Given
    PlayerTemplate playerTemplate = new PlayerTemplate();
    playerTemplate.setHealth(100);
    Player player = new Player(playerTemplate);
    player.setHealth(65);

    //When
    String result = DisplayUtils.formattedHealth(player);

    //Then
    Assert.assertEquals("65/100", result);
  }

  @Test
  public void formattedHealthForEnemy() {
    //Given
    EnemyTemplate template = new EnemyTemplate();
    template.setHealth(100);
    Enemy enemy = new Enemy(template);
    enemy.setHealth(77);

    //When
    String result = DisplayUtils.formattedHealth(enemy);

    //Then
    Assert.assertEquals("77/100", result);
  }
}