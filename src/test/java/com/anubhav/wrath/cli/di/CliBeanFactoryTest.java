package com.anubhav.wrath.cli.di;

import static org.junit.Assert.assertNotNull;

import com.anubhav.wrath.cli.config.CliConfig;
import com.anubhav.wrath.cli.screen.WelcomeScreen;
import com.anubhav.wrath.core.exception.DIException;
import org.junit.Test;

public class CliBeanFactoryTest {

  @Test
  public void validBeanRequest() {
    assertNotNull(CliBeanFactory.getBean(WelcomeScreen.class));
  }

  @Test(expected = DIException.class)
  public void invalidBeanRequest() {
    CliBeanFactory.getBean(CliConfig.class);
  }
}