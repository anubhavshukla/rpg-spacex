package com.anubhav.wrath.cli;

import com.anubhav.wrath.cli.config.CliConfig;
import com.anubhav.wrath.cli.config.MenuConfig;
import com.anubhav.wrath.cli.di.CliBeanFactory;
import com.anubhav.wrath.cli.screen.WelcomeScreen;
import com.anubhav.wrath.core.BootstrapCore;

/**
 * Main class to run CLI interface
 */
public class GameLauncher {

  public static void main(String[] args) {
    BootstrapCore.loadData();
    start();
  }

  private static void start() {
    configureApp();
    CliBeanFactory.getBean(WelcomeScreen.class).render(null);
  }

  private static void configureApp() {
    CliConfig[] cliConfigs = {
        new MenuConfig()
    };

    for (CliConfig cliConfig : cliConfigs) {
      cliConfig.configure();
    }
  }
}
