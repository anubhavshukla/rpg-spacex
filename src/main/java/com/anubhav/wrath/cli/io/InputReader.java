package com.anubhav.wrath.cli.io;

/**
 * Definition for Input reader.
 */
public interface InputReader {

  /**
   * Method called to read input value.
   */
  String read();

  /**
   * Should be called before application exit to close reader.
   */
  void close();
}
