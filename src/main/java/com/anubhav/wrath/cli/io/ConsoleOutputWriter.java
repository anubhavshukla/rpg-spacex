package com.anubhav.wrath.cli.io;

import com.anubhav.wrath.core.i18n.MessageResource;

public class ConsoleOutputWriter implements OutputWriter {

  public void write(String message) {
    System.out.println(message);
  }

  public void separator() {
    System.out.println(MessageResource.getMessage("screen.separator"));
  }
}
