package com.anubhav.wrath.cli.io;

import com.anubhav.wrath.core.exception.GameException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Standard io based implementation for InputReader
 */
public class ConsoleInputReader implements InputReader {

  private BufferedReader br = null;

  public ConsoleInputReader() {
    init();
  }

  public void init() {
    br = new BufferedReader(new InputStreamReader(System.in));
  }


  public String read() {
    try {
      return br.readLine();
    } catch (IOException e) {
      e.printStackTrace();
      throw new GameException(e.getMessage());
    }
  }

  public void close() {
    if (br != null) {
      try {
        br.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
