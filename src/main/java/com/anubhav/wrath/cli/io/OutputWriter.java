package com.anubhav.wrath.cli.io;

/**
 * Definition for writer to output stream.
 */
public interface OutputWriter {

  /**
   * Write message to output
   */
  void write(String message);

  /**
   * Write standard separator line.
   */
  void separator();
}
