package com.anubhav.wrath.cli.di;

import com.anubhav.wrath.cli.io.ConsoleInputReader;
import com.anubhav.wrath.cli.io.ConsoleOutputWriter;
import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.cli.listener.BackToMainMenuListener;
import com.anubhav.wrath.cli.listener.ChangeAttackWeaponMenuListener;
import com.anubhav.wrath.cli.listener.ChangeDefenseWeaponMenuListener;
import com.anubhav.wrath.cli.listener.ExitGameMenuListener;
import com.anubhav.wrath.cli.listener.GameRulesListener;
import com.anubhav.wrath.cli.listener.LoadGameMenuListener;
import com.anubhav.wrath.cli.listener.NewGameMenuListener;
import com.anubhav.wrath.cli.listener.SaveSessionMenuListener;
import com.anubhav.wrath.cli.listener.StartMissionMenuListener;
import com.anubhav.wrath.cli.listener.StrikeEnemyMenuListener;
import com.anubhav.wrath.cli.screen.ChangeAttackWeaponScreen;
import com.anubhav.wrath.cli.screen.ChangeDefenseWeaponScreen;
import com.anubhav.wrath.cli.screen.CreatePlayerScreen;
import com.anubhav.wrath.cli.screen.ExitGameScreen;
import com.anubhav.wrath.cli.screen.FightInprogressScreen;
import com.anubhav.wrath.cli.screen.FightLostScreen;
import com.anubhav.wrath.cli.screen.FightWonScreen;
import com.anubhav.wrath.cli.screen.GameRulesScreen;
import com.anubhav.wrath.cli.screen.GameWonScreen;
import com.anubhav.wrath.cli.screen.LoadSessionScreen;
import com.anubhav.wrath.cli.screen.StartNewGameScreen;
import com.anubhav.wrath.cli.screen.WelcomeScreen;
import com.anubhav.wrath.core.di.BeanFactory;
import com.anubhav.wrath.core.exception.DIException;
import com.anubhav.wrath.core.service.FightService;
import com.anubhav.wrath.core.service.GameSessionService;
import com.anubhav.wrath.core.service.PlayerService;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of simple bean factory for Cli weaving. Eagerly creates beans at start time.
 *
 * TODO-Can be replaced by any IOC framework like Spring.
 */
public class CliBeanFactory extends BeanFactory {

  private static final Map<Class, Object> beanStore = new HashMap<>();

  static {
    init();
  }

  private static void init() {
    addBean(OutputWriter.class, ConsoleOutputWriter.class);
    addBean(InputReader.class, ConsoleInputReader.class);

    addBean(WelcomeScreen.class,
        new WelcomeScreen(getBean(OutputWriter.class), getBean(InputReader.class)));
    addBean(ExitGameScreen.class,
        new ExitGameScreen(getBean(OutputWriter.class), getBean(InputReader.class)));
    addBean(StartNewGameScreen.class,
        new StartNewGameScreen(getBean(OutputWriter.class), getBean(InputReader.class)));
    addBean(LoadSessionScreen.class,
        new LoadSessionScreen(getBean(OutputWriter.class), getBean(InputReader.class),
            BeanFactory.getBean(GameSessionService.class), getBean(WelcomeScreen.class)));
    addBean(CreatePlayerScreen.class,
        new CreatePlayerScreen(getBean(OutputWriter.class), getBean(InputReader.class),
            BeanFactory.getBean(PlayerService.class), getBean(StartNewGameScreen.class)));
    addBean(GameRulesScreen.class,
        new GameRulesScreen(getBean(OutputWriter.class), getBean(InputReader.class),
            getBean(WelcomeScreen.class)));
    addBean(FightInprogressScreen.class,
        new FightInprogressScreen(getBean(OutputWriter.class), getBean(InputReader.class)));
    addBean(FightWonScreen.class,
        new FightWonScreen(getBean(OutputWriter.class), getBean(InputReader.class)));
    addBean(GameWonScreen.class,
        new GameWonScreen(getBean(OutputWriter.class), getBean(InputReader.class),
            getBean(WelcomeScreen.class)));
    addBean(FightLostScreen.class,
        new FightLostScreen(getBean(OutputWriter.class), getBean(InputReader.class),
            getBean(WelcomeScreen.class)));
    addBean(ChangeAttackWeaponScreen.class,
        new ChangeAttackWeaponScreen(getBean(OutputWriter.class), getBean(InputReader.class),
            BeanFactory.getBean(PlayerService.class), getBean(FightInprogressScreen.class)));
    addBean(ChangeDefenseWeaponScreen.class,
        new ChangeDefenseWeaponScreen(getBean(OutputWriter.class), getBean(InputReader.class),
            BeanFactory.getBean(PlayerService.class), getBean(FightInprogressScreen.class)));

    addBean(NewGameMenuListener.class,
        new NewGameMenuListener(BeanFactory.getBean(GameSessionService.class),
            getBean(CreatePlayerScreen.class)));
    addBean(ExitGameMenuListener.class, new ExitGameMenuListener(getBean(ExitGameScreen.class)));
    addBean(StartMissionMenuListener.class, new StartMissionMenuListener(BeanFactory.getBean(
        FightService.class), getBean(FightInprogressScreen.class)));
    addBean(BackToMainMenuListener.class, new BackToMainMenuListener(getBean(WelcomeScreen.class)));
    addBean(SaveSessionMenuListener.class,
        new SaveSessionMenuListener(BeanFactory.getBean(GameSessionService.class),
            getBean(WelcomeScreen.class)));
    addBean(GameRulesListener.class, new GameRulesListener(getBean(GameRulesScreen.class)));
    addBean(LoadGameMenuListener.class, new LoadGameMenuListener(getBean(LoadSessionScreen.class)));
    addBean(StrikeEnemyMenuListener.class,
        new StrikeEnemyMenuListener(BeanFactory.getBean(FightService.class),
            getBean(GameWonScreen.class), getBean(FightWonScreen.class),
            getBean(FightLostScreen.class), getBean(FightInprogressScreen.class)));
    addBean(ChangeAttackWeaponMenuListener.class,
        new ChangeAttackWeaponMenuListener(getBean(ChangeAttackWeaponScreen.class)));
    addBean(ChangeDefenseWeaponMenuListener.class,
        new ChangeDefenseWeaponMenuListener(getBean(ChangeDefenseWeaponScreen.class)));
  }

  private static void addBean(Class className, Object object) {
    beanStore.put(className, object);
  }

  private static void addBean(Class className, Class implementationClass) {
    try {
      Object bean = implementationClass.newInstance();
      beanStore.put(className, bean);
    } catch (InstantiationException | IllegalAccessException e) {
      throw new DIException("Unable to initialize bean");
    }
  }

  @SuppressWarnings("unchecked")
  public static <T> T getBean(Class<T> clazz) {
    T bean;

    try {
      bean = (T) beanStore.get(clazz);
    } catch (Throwable t) {
      throw new DIException("Unable to initialize bean");
    }

    if (null == bean) {
      throw new DIException("Bean not found.");
    }

    return bean;
  }
}
