package com.anubhav.wrath.cli.config;

/**
 * CLI interface configuration definition. All implementation of this class are loaded at start time
 * by GameLauncher.
 */
public interface CliConfig {

  void configure();
}
