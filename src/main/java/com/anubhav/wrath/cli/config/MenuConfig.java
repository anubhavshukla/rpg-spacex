package com.anubhav.wrath.cli.config;


import static com.anubhav.wrath.cli.di.CliBeanFactory.getBean;

import com.anubhav.wrath.cli.listener.ChangeAttackWeaponMenuListener;
import com.anubhav.wrath.cli.listener.ChangeDefenseWeaponMenuListener;
import com.anubhav.wrath.cli.listener.ExitGameMenuListener;
import com.anubhav.wrath.cli.listener.GameRulesListener;
import com.anubhav.wrath.cli.listener.LoadGameMenuListener;
import com.anubhav.wrath.cli.listener.NewGameMenuListener;
import com.anubhav.wrath.cli.listener.SaveSessionMenuListener;
import com.anubhav.wrath.cli.listener.StartMissionMenuListener;
import com.anubhav.wrath.cli.listener.StrikeEnemyMenuListener;
import com.anubhav.wrath.cli.menu.Menu;
import com.anubhav.wrath.cli.menu.MenuBuilder;
import com.anubhav.wrath.cli.menu.MenuItem;
import com.anubhav.wrath.cli.menu.MenuItemBuilder;
import com.anubhav.wrath.cli.store.MenuStore;

/**
 * Configure menus in application. This is programmatic configuration. But same thing can be
 * achieved by reading a csv or xml or json.
 */
public class MenuConfig implements CliConfig {

  public void configure() {
    createMainMenu();
    createPlayerMenu();
    createFightMenu();
    createFightWonMenu();
  }

  private void createMainMenu() {
    MenuItem[] menuItems = {
        MenuItemBuilder.getInstance().id("mainmenu-001").shortCode("1")
            .message("menu.main.startnewgame")
            .listener(getBean(NewGameMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("mainmenu-002").shortCode("2")
            .message("menu.main.loadgame")
            .listener(getBean(LoadGameMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("mainmenu-003").shortCode("3")
            .message("menu.main.explore").listener(getBean(GameRulesListener.class)).build(),
        MenuItemBuilder.getInstance().id("mainmenu-004").shortCode("4").message("menu.main.exit")
            .listener(getBean(ExitGameMenuListener.class)).build(),
    };
    Menu menu = MenuBuilder.getInstance().id("main-menu").greeting("menu.main.message")
        .menuItems(menuItems).build();
    MenuStore.put(menu.getId(), menu);
  }

  private void createPlayerMenu() {
    MenuItem[] menuItems = {
        MenuItemBuilder.getInstance().id("player-menu-001").shortCode("a")
            .message("menu.player.startmission")
            .listener(getBean(StartMissionMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("player-menu-003").shortCode("b")
            .message("menu.main.explore").listener(getBean(GameRulesListener.class)).build(),
        MenuItemBuilder.getInstance().id("player-menu-004").shortCode("c")
            .message("menu.player.savemission")
            .listener(getBean(SaveSessionMenuListener.class)).build(),
    };
    Menu menu = MenuBuilder.getInstance().id("player-menu")
        .greeting("menu.player.message").menuItems(menuItems).build();
    MenuStore.put(menu.getId(), menu);
  }

  private void createFightMenu() {
    MenuItem[] menuItems = {
        MenuItemBuilder.getInstance().id("fight-menu-001").shortCode("a")
            .message("menu.fight.attack")
            .listener(getBean(StrikeEnemyMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("fight-menu-003").shortCode("b")
            .message("menu.fight.changeattackweapon")
            .listener(getBean(ChangeAttackWeaponMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("fight-menu-004").shortCode("c")
            .message("menu.fight.changedefenseweapon")
            .listener(getBean(ChangeDefenseWeaponMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("fight-menu-005").shortCode("d")
            .message("menu.fight.savemission")
            .listener(getBean(SaveSessionMenuListener.class)).build(),
    };
    Menu menu = MenuBuilder.getInstance().id("fight-menu").greeting("menu.fight.message")
        .menuItems(menuItems).build();
    MenuStore.put(menu.getId(), menu);
  }

  private void createFightWonMenu() {
    MenuItem[] menuItems = {
        MenuItemBuilder.getInstance().id("fight-won-menu-001").shortCode("a")
            .message("menu.fightwon.continue")
            .listener(getBean(StartMissionMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("fight-won-menu-003").shortCode("b")
            .message("menu.fight.changeattackweapon")
            .listener(getBean(ChangeAttackWeaponMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("fight-won-menu-004").shortCode("c")
            .message("menu.fight.changedefenseweapon")
            .listener(getBean(ChangeDefenseWeaponMenuListener.class)).build(),
        MenuItemBuilder.getInstance().id("fight-won-menu-005").shortCode("d")
            .message("menu.fight.savemission")
            .listener(getBean(SaveSessionMenuListener.class)).build(),
    };
    Menu menu = MenuBuilder.getInstance().id("fight-won-menu")
        .greeting("menu.fightwon.message").menuItems(menuItems).build();
    MenuStore.put(menu.getId(), menu);
  }
}
