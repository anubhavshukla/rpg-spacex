package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.WelcomeScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.GameSessionService;

/**
 * Listener for menu option to save current game session.
 */
public class SaveSessionMenuListener implements MenuItemListener {

  private GameSessionService gameSessionService;

  private WelcomeScreen welcomeScreen;

  public SaveSessionMenuListener(GameSessionService gameSessionService,
      WelcomeScreen welcomeScreen) {
    this.gameSessionService = gameSessionService;
    this.welcomeScreen = welcomeScreen;
  }

  @Override
  public void execute(GameSession session) {
    gameSessionService.save(session);
    welcomeScreen.render(null);
  }
}
