package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.CreatePlayerScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.GameSessionService;

/**
 * Listener for menu option to start a new game.
 */
public class NewGameMenuListener implements MenuItemListener {

  private GameSessionService gameSessionService;

  private CreatePlayerScreen createPlayerScreen;

  public NewGameMenuListener(GameSessionService gameSessionService,
      CreatePlayerScreen createPlayerScreen) {
    this.gameSessionService = gameSessionService;
    this.createPlayerScreen = createPlayerScreen;
  }

  @Override
  public void execute(GameSession session) {
    GameSession newSession = gameSessionService.startNew();
    createPlayerScreen.render(newSession);
  }
}
