package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.FightInprogressScreen;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.FightService;

/**
 * Listener for menu item to start a fight with an enemy.
 */
public class StartMissionMenuListener implements MenuItemListener {

  private FightService fightService;

  private FightInprogressScreen fightInprogressScreen;

  public StartMissionMenuListener(FightService fightService,
      FightInprogressScreen fightInprogressScreen) {
    this.fightService = fightService;
    this.fightInprogressScreen = fightInprogressScreen;
  }

  @Override
  public void execute(GameSession session) {
    fightService.startFight(session);
    fightInprogressScreen.render(session);
  }
}
