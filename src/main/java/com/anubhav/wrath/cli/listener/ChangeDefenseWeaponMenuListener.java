package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.ChangeDefenseWeaponScreen;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Listener for option to change defense weapon for a Player.
 */
public class ChangeDefenseWeaponMenuListener implements MenuItemListener {

  private ChangeDefenseWeaponScreen changeDefenseWeaponScreen;

  public ChangeDefenseWeaponMenuListener(
      ChangeDefenseWeaponScreen changeDefenseWeaponScreen) {
    this.changeDefenseWeaponScreen = changeDefenseWeaponScreen;
  }

  @Override
  public void execute(GameSession session) {
    changeDefenseWeaponScreen.render(session);
  }
}
