package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.GameRulesScreen;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Listener for menu item take a tour of the game.
 */
public class GameRulesListener implements MenuItemListener {

  private GameRulesScreen gameRulesScreen;

  public GameRulesListener(GameRulesScreen gameRulesScreen) {
    this.gameRulesScreen = gameRulesScreen;
  }

  @Override
  public void execute(GameSession session) {
    gameRulesScreen.render(session);
  }
}
