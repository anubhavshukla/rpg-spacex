package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.WelcomeScreen;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Listener for menu option to go back to main menu.
 */
public class BackToMainMenuListener implements MenuItemListener {

  private WelcomeScreen welcomeScreen;

  public BackToMainMenuListener(WelcomeScreen welcomeScreen) {
    this.welcomeScreen = welcomeScreen;
  }

  @Override
  public void execute(GameSession session) {
    welcomeScreen.render(session);
  }
}
