package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.FightInprogressScreen;
import com.anubhav.wrath.cli.screen.FightLostScreen;
import com.anubhav.wrath.cli.screen.FightWonScreen;
import com.anubhav.wrath.cli.screen.GameWonScreen;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.GameStatusEnum;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.FightService;

/**
 * Listener for menu item to attack an enemy.
 */
public class StrikeEnemyMenuListener implements MenuItemListener {

  private FightService fightService;

  private GameWonScreen gameWonScreen;

  private FightWonScreen fightWonScreen;

  private FightLostScreen fightLostScreen;

  private FightInprogressScreen fightInprogressScreen;

  public StrikeEnemyMenuListener(FightService fightService, GameWonScreen gameWonScreen,
      FightWonScreen fightWonScreen, FightLostScreen fightLostScreen,
      FightInprogressScreen fightInprogressScreen) {
    this.fightService = fightService;
    this.gameWonScreen = gameWonScreen;
    this.fightWonScreen = fightWonScreen;
    this.fightLostScreen = fightLostScreen;
    this.fightInprogressScreen = fightInprogressScreen;
  }

  @Override
  public void execute(GameSession session) {
    GameSession gameSession = fightService.executeAttack(session);
    if (GameStatusEnum.COMPLETED == gameSession.getGameStatus()) {
      gameWonScreen.render(gameSession);
    } else if (FightStatusEnum.WON == gameSession.getFightStatus()) {
      fightWonScreen.render(gameSession);
    } else if (FightStatusEnum.LOST == gameSession.getFightStatus()) {
      fightLostScreen.render(gameSession);
    } else {
      fightInprogressScreen.render(gameSession);
    }
  }
}
