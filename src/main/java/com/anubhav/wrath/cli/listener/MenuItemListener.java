package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Definition of MenuItemListener On selecting a MenuItem corresponding MenuItemListener is
 * invoked.
 */
public interface MenuItemListener {

  /**
   * Method called on selection of MenuItem
   */
  void execute(GameSession session);
}
