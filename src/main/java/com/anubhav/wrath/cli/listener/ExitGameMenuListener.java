package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.ExitGameScreen;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Listener for menu option to exit the game.
 */
public class ExitGameMenuListener implements MenuItemListener {

  private ExitGameScreen exitGameScreen;

  public ExitGameMenuListener(ExitGameScreen exitGameScreen) {
    this.exitGameScreen = exitGameScreen;
  }

  @Override
  public void execute(GameSession session) {
    exitGameScreen.render(session);
  }
}
