package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.LoadSessionScreen;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Listener for menu option to load previously stored games sessions.
 */
public class LoadGameMenuListener implements MenuItemListener {

  private LoadSessionScreen loadSessionScreen;

  public LoadGameMenuListener(LoadSessionScreen loadSessionScreen) {
    this.loadSessionScreen = loadSessionScreen;
  }

  @Override
  public void execute(GameSession session) {
    loadSessionScreen.render(session);
  }
}
