package com.anubhav.wrath.cli.listener;

import com.anubhav.wrath.cli.screen.ChangeAttackWeaponScreen;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Listener for menu option to change attack weapon of a Player
 */
public class ChangeAttackWeaponMenuListener implements MenuItemListener {

  private ChangeAttackWeaponScreen changeAttackWeaponScreen;

  public ChangeAttackWeaponMenuListener(
      ChangeAttackWeaponScreen changeAttackWeaponScreen) {
    this.changeAttackWeaponScreen = changeAttackWeaponScreen;
  }

  @Override
  public void execute(GameSession session) {
    changeAttackWeaponScreen.render(session);
  }
}
