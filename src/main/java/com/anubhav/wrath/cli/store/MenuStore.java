package com.anubhav.wrath.cli.store;

import com.anubhav.wrath.cli.menu.Menu;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * In-memory store or cache for Menu and MenuItems.
 */
public final class MenuStore {

  private static Map<String, Menu> menus = new HashMap<>();

  private MenuStore() {
    //initialization not allowed
  }

  public static void put(String name, Menu menu) {
    menus.put(name, menu);
  }

  public static Optional<Menu> get(String name) {
    return Optional.ofNullable(menus.get(name));
  }
}
