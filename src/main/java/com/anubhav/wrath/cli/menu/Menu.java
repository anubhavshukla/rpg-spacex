package com.anubhav.wrath.cli.menu;

/**
 * Model class that defines a menu.
 */
public class Menu {

  private final String id;
  private final String greeting;
  private final MenuItem[] menuItems;

  public Menu(String id, String greeting, MenuItem[] menuItems) {
    this.id = id;
    this.greeting = greeting;
    this.menuItems = menuItems;
  }

  public String getId() {
    return id;
  }

  public String getGreeting() {
    return greeting;
  }

  public MenuItem[] getMenuItems() {
    return menuItems;
  }
}
