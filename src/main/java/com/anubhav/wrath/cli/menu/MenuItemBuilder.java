package com.anubhav.wrath.cli.menu;

import com.anubhav.wrath.cli.listener.MenuItemListener;
import com.anubhav.wrath.core.i18n.MessageResource;

/**
 * Builder class for MenuItem
 */
public class MenuItemBuilder {

  private String id;
  private String shortCode;
  private String message;
  private MenuItemListener menuItemListener;

  private MenuItemBuilder() {
  }

  public static MenuItemBuilder getInstance() {
    return new MenuItemBuilder();
  }

  public MenuItemBuilder id(String id) {
    this.id = id;
    return this;
  }

  public MenuItemBuilder shortCode(String shortCode) {
    this.shortCode = shortCode;
    return this;
  }

  public MenuItemBuilder message(String message) {
    this.message = MessageResource.getMessage(message);
    return this;
  }

  public MenuItemBuilder listener(MenuItemListener listener) {
    this.menuItemListener = listener;
    return this;
  }

  public MenuItem build() {
    return new MenuItem(id, shortCode, message, menuItemListener);
  }

}
