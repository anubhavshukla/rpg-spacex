package com.anubhav.wrath.cli.menu;

import com.anubhav.wrath.core.i18n.MessageResource;

/**
 * Builder class for the Menu.
 */
public class MenuBuilder {

  private String id;
  private String greeting;
  private MenuItem[] menuItems;

  private MenuBuilder() {
  }

  public static MenuBuilder getInstance() {
    return new MenuBuilder();
  }

  public MenuBuilder id(String id) {
    this.id = id;
    return this;
  }

  public MenuBuilder greeting(String greeting) {
    this.greeting = MessageResource.getMessage(greeting);
    return this;
  }

  public MenuBuilder menuItems(MenuItem[] menuItems) {
    this.menuItems = menuItems;
    return this;
  }

  public Menu build() {
    return new Menu(id, greeting, menuItems);
  }
}
