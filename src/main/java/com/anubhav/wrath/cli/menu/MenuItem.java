package com.anubhav.wrath.cli.menu;

import com.anubhav.wrath.cli.listener.MenuItemListener;

/**
 * Model class that defines each menu item in a menu.
 */
public class MenuItem {

  private final String id;
  private final String shortCode;
  private final String message;
  private final MenuItemListener listener;

  MenuItem(String id, String shortCode, String message, MenuItemListener listener) {
    this.id = id;
    this.shortCode = shortCode;
    this.message = message;
    this.listener = listener;
  }

  public String getId() {
    return id;
  }

  public String getShortCode() {
    return shortCode;
  }

  public String getMessage() {
    return message;
  }

  public MenuItemListener getListener() {
    return listener;
  }
}
