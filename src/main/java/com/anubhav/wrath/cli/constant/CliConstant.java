package com.anubhav.wrath.cli.constant;

/**
 * Cli interface constants.
 */
public interface CliConstant {

  String MAIN_MENU_ID = "main-menu";
  String PLAYER_MENU_ID = "player-menu";
  String FIGHT_MENU_ID = "fight-menu";
  String FIGHT_WON_MENU_ID = "fight-won-menu";
}
