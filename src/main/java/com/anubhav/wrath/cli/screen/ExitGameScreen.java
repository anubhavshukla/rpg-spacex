package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Implementation of screen for exit from the game.
 */
public class ExitGameScreen extends GameScreen {

  public ExitGameScreen(OutputWriter outputWriter,
      InputReader inputReader) {
    super(outputWriter, inputReader);
  }

  @Override
  protected void outputBody(GameSession session) {
    inputReader.close();
    writeMessage("screen.exitgame.message");
    outputSeparator();
  }

  @Override
  public String getHeaderMessage() {
    return MessageResource.getMessage("screen.exitgame.header");
  }
}
