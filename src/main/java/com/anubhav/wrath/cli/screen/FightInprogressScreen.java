package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.cli.constant.CliConstant.FIGHT_MENU_ID;
import static com.anubhav.wrath.cli.utils.DisplayUtils.formattedHealth;
import static com.anubhav.wrath.cli.utils.DisplayUtils.weaponWithPower;
import static com.anubhav.wrath.cli.utils.StringUtils.showInColumns;
import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.exception.InvalidStateException;
import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Implementation of screen for in-progress fight between Player and Enemy
 */
public class FightInprogressScreen extends MenuBasedScreen {

  public FightInprogressScreen(OutputWriter outputWriter,
      InputReader inputReader) {
    super(outputWriter, inputReader);
  }

  @Override
  protected String getMenuId() {
    return FIGHT_MENU_ID;
  }

  @Override
  protected void outputBody(GameSession session) {
    if (session == null || session.getPlayer() == null || session.getActiveEnemy() == null) {
      throw new InvalidStateException(
          getMessage("generic.invalid.session"));
    }

    Enemy activeEnemy = session.getActiveEnemy();
    Player player = session.getPlayer();

    write(
        showInColumns(getMessage("screen.fightinprogress.stats"),
            getMessage("screen.fightinprogress.player"),
            getMessage("screen.fightinprogress.enemy"))
    );
    outputSeparator();

    write(
        showInColumns(getMessage("screen.fightinprogress.name"),
            player.getName(), activeEnemy.getName()));
    write(
        showInColumns(getMessage("screen.fightinprogress.health"),
            formattedHealth(player), formattedHealth(activeEnemy)));
    write(
        showInColumns(getMessage("screen.fightinprogress.attackweapon"),
            weaponWithPower(player.getActiveAttackWeapon()),
            weaponWithPower(activeEnemy.getTemplate().getAttackWeapon())));
    write(
        showInColumns(getMessage("screen.fightinprogress.defenseweapon"),
            weaponWithPower(player.getActiveDefenseWeapon()),
            weaponWithPower(activeEnemy.getTemplate().getDefenseWeapon())));
  }

  @Override
  protected String getHeaderMessage() {
    return getMessage("screen.fightinprogress.header");
  }
}
