package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.cli.utils.DisplayUtils;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.PlayerService;

/**
 * Implementation of screen for changing of defense weapon for Player.
 */
public class ChangeDefenseWeaponScreen extends GameScreen {

  private PlayerService playerService;

  private FightInprogressScreen fightInprogressScreen;

  public ChangeDefenseWeaponScreen(OutputWriter outputWriter,
      InputReader inputReader, PlayerService playerService,
      FightInprogressScreen fightInprogressScreen) {
    super(outputWriter, inputReader);
    this.playerService = playerService;
    this.fightInprogressScreen = fightInprogressScreen;
  }

  @Override
  protected void outputBody(GameSession session) {
    writeMessage("screen.changedefenseweapon.message1");
    final int[] i = {1};

    Player player = session.getPlayer();
    player.getDefenseWeapons().forEach(defenseWeapon -> {
      write("(" + i[0] + ") " + DisplayUtils.weaponWithPower(defenseWeapon));
      i[0]++;
    });
    writeMessage("screen.changedefenseweapon.return.message");
    outputSeparator();

    writeMessage("screen.changedefenseweapon.selection.message");
    String selection;
    do {
      selection = inputReader.read();
    } while (!isValidSelection(selection, i[0]));

    if (!selection.equals("0")) {
      setNewDefenseWeapon(session, player, selection);
    }
    fightInprogressScreen.render(session);
  }

  private void setNewDefenseWeapon(GameSession session, Player player, String selection) {
    String templateId = player.getDefenseWeapons().get(Integer.valueOf(selection) - 1)
        .getTemplateId();
    Player playerNew = playerService.setActiveDefenseWeapon(player, templateId);
    session.setPlayer(playerNew);
  }

  /**
   * Validate if correct weapon option was selected
   */
  private boolean isValidSelection(String selection, int maxValue) {
    try {
      int selectedValue = Integer.valueOf(selection);
      if (selectedValue >= 0 && selectedValue < maxValue) {
        return true;
      }
    } catch (Exception e) {
      // do nothing - invalid input
    }
    writeMessage("generic.invalid.input.message");
    return false;
  }

  @Override
  protected String getHeaderMessage() {
    return MessageResource.getMessage("screen.changedefenseweapon.header");
  }
}
