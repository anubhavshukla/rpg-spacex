package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.exception.InvalidStateException;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Implementation of screen for scenario where Player has lost the fight.
 */
public class FightLostScreen extends GameScreen {

  private WelcomeScreen welcomeScreen;

  public FightLostScreen(OutputWriter outputWriter,
      InputReader inputReader, WelcomeScreen welcomeScreen) {
    super(outputWriter, inputReader);
    this.welcomeScreen = welcomeScreen;
  }

  @Override
  protected void outputBody(GameSession session) {
    if (session == null || session.getPlayer() == null) {
      throw new InvalidStateException(
          MessageResource.getMessage("generic.invalid.session"));
    }

    if (FightStatusEnum.LOST != session.getFightStatus()) {
      throw new InvalidStateException(MessageResource.getMessage("screen.fightlost.notcomplete"));
    }

    writeMessage("screen.fightlost.message");
    outputSeparator();
    moveOnEnter(session, welcomeScreen);
  }

  @Override
  protected String getHeaderMessage() {
    return MessageResource.getMessage("screen.fightlost.header");
  }
}
