package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.cli.utils.DisplayUtils.weaponWithPower;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.PlayerService;

/**
 * Implementation of screen for changing attack weapon of the Player.
 */
public class ChangeAttackWeaponScreen extends GameScreen {

  private PlayerService playerService;

  private FightInprogressScreen fightInprogressScreen;

  public ChangeAttackWeaponScreen(OutputWriter outputWriter,
      InputReader inputReader, PlayerService playerService,
      FightInprogressScreen fightInprogressScreen) {
    super(outputWriter, inputReader);
    this.playerService = playerService;
    this.fightInprogressScreen = fightInprogressScreen;
  }

  @Override
  protected void outputBody(GameSession session) {
    writeMessage("screen.changeattackweapon.message1");
    final int[] i = {1};

    Player player = session.getPlayer();
    player.getAttackWeapons().forEach(attackWeapon -> {
      write("(" + i[0] + ") " + weaponWithPower(attackWeapon));
      i[0]++;
    });
    writeMessage("screen.changeattackweapon.return.message");
    outputSeparator();

    writeMessage("screen.changeattackweapon.selection.message");
    String selection;
    do {
      selection = inputReader.read();
    } while (!isValidSelection(selection, i[0]));

    if (!selection.equals("0")) {
      String templateId = player.getAttackWeapons().get(Integer.valueOf(selection) - 1)
          .getTemplateId();
      Player playerNew = playerService.setActiveAttackWeapon(player, templateId);
      session.setPlayer(playerNew);
    }
    fightInprogressScreen.render(session);
  }

  /**
   * Validate if correct weapon option was selected.
   */
  private boolean isValidSelection(String selection, int maxValue) {
    try {
      int selectedValue = Integer.valueOf(selection);
      if (selectedValue >= 0 && selectedValue < maxValue) {
        return true;
      }
    } catch (Exception e) {
      // do nothing - invalid input
    }
    writeMessage("generic.invalid.input.message");
    return false;
  }

  @Override
  protected String getHeaderMessage() {
    return MessageResource.getMessage("screen.changeattackweapon.header");
  }
}
