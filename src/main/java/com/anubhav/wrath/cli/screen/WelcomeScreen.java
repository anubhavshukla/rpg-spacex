package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.constant.CliConstant;
import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Welcome screen of the game.
 */
public class WelcomeScreen extends MenuBasedScreen {

  public WelcomeScreen(OutputWriter outputWriter,
      InputReader inputReader) {
    super(outputWriter, inputReader);
  }

  @Override
  protected void outputBody(GameSession session) {
    writeMessage("screen.welcome.message");
  }

  @Override
  public String getMenuId() {
    return CliConstant.MAIN_MENU_ID;
  }

  @Override
  public String getHeaderMessage() {
    return MessageResource.getMessage("screen.welcome.header");
  }
}
