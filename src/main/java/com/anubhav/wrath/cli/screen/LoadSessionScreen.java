package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.di.CliBeanFactory;
import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.GameSessionService;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of screen for loading stored sessions.
 */
public class LoadSessionScreen extends GameScreen {

  private GameSessionService gameSessionService;

  private WelcomeScreen welcomeScreen;

  public LoadSessionScreen(OutputWriter outputWriter,
      InputReader inputReader, GameSessionService gameSessionService,
      WelcomeScreen welcomeScreen) {
    super(outputWriter, inputReader);
    this.gameSessionService = gameSessionService;
    this.welcomeScreen = welcomeScreen;
  }

  @Override
  public String getHeaderMessage() {
    return MessageResource.getMessage("screen.loadsession.header");
  }

  @Override
  public void outputBody(GameSession session) {
    List<GameSession> savedSessions = gameSessionService.findAll();
    if (!savedSessions.isEmpty()) {
      displayStoreSessions(session, savedSessions);
    } else {
      displayNoSessions(session);
    }
  }

  private void displayNoSessions(GameSession session) {
    writeMessage("screen.loadsession.nosession");
    outputNewLine();
    outputSeparator();
    writeMessage("screen.loadsession.nosession.exit");
    inputReader.read();
    welcomeScreen.render(session);
  }

  private void displayStoreSessions(GameSession session, List<GameSession> savedSessions) {
    writeMessage("screen.loadsession.message1");
    final int[] i = {1};
    savedSessions.forEach(savedSession -> {
      write(
          i[0] + ". " + savedSession.getPlayer().getName() + " - " + savedSession.getName());
      i[0]++;
    });
    writeMessage("screen.loadsession.goback.option");

    String selection;
    do {
      selection = inputReader.read();
    } while (!isValidSelection(selection, i[0]));

    if (selection.equals("0")) {
      welcomeScreen.render(session);
    } else {
      GameSession loadedSession = new ArrayList<>(savedSessions)
          .get(Integer.valueOf(selection) - 1);
      CliBeanFactory.getBean(loadedSession.getCurrentScreen()).render(loadedSession);
    }
  }

  /**
   * Validate selected session option.
   */
  private boolean isValidSelection(String selection, int maxValue) {
    try {
      int selectedValue = Integer.valueOf(selection);
      if (selectedValue >= 0 && selectedValue < maxValue) {
        return true;
      }
    } catch (Exception e) {
      // do nothing - invalid input
    }
    writeMessage("generic.invalid.input.message");
    return false;
  }
}
