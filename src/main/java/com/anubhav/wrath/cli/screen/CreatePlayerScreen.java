package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.service.PlayerService;

/**
 * Implementation of screen for creating new Player with name
 */
public class CreatePlayerScreen extends GameScreen {

  private PlayerService playerService;

  private StartNewGameScreen startNewGameScreen;

  public CreatePlayerScreen(OutputWriter outputWriter, InputReader inputReader,
      PlayerService playerService, StartNewGameScreen startNewGameScreen) {
    super(outputWriter, inputReader);
    this.playerService = playerService;
    this.startNewGameScreen = startNewGameScreen;
  }

  @Override
  protected void outputBody(GameSession session) {
    writeMessage("screen.createplayer.message1");
    outputNewLine();
    outputSeparator();

    String playerName = readNonEmptyInput();

    Player player = playerService.create(playerName);
    session.setPlayer(player);
    startNewGameScreen.render(session);
  }

  @Override
  public String getHeaderMessage() {
    return MessageResource.getMessage("screen.createplayer.header");
  }

  /**
   * Read entered Player name
   */
  private String readNonEmptyInput() {
    String inputValue = "";
    do {
      inputValue = inputReader.read();
    } while (isValidNonEmptyInput(inputValue));
    return inputValue;
  }

  /**
   * Validate if valid Player name was entered
   */
  private boolean isValidNonEmptyInput(String inputValue) {
    if (inputValue.trim().equals("")) {
      writeMessage("screen.createplayer.invalid.name");
      return true;
    }
    return false;
  }
}
