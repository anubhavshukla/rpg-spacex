package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.cli.utils.DisplayUtils.formattedHealth;
import static com.anubhav.wrath.cli.utils.StringUtils.appendAtLocation;
import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;
import static java.lang.String.valueOf;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Abstract implementation of a Game screen. Divides screen into 2 parts: top header and bottom
 * body.
 */
public abstract class GameScreen {

  private static final String HEADER_PLAYER_PREFIX = getMessage("game.header.player") + " ";
  private static final String HEADER_PLAYER_GUEST = getMessage("game.header.player.guest");
  private static final int PLAYER_STATS_SEPARATION = 20;

  OutputWriter outputWriter;
  InputReader inputReader;

  public GameScreen(OutputWriter outputWriter, InputReader inputReader) {
    this.outputWriter = outputWriter;
    this.inputReader = inputReader;
  }

  /**
   * If true current screen class is stored in GameSession object. Current screen class is used to
   * re-render stored session.
   */
  boolean storeCurrentScreen() {
    return true;
  }

  /**
   * General method to output text to Std.out.
   */
  void write(String message) {
    outputWriter.write(message);
  }

  /**
   * Print a blank new line.
   */
  void outputNewLine() {
    outputWriter.write("");
  }

  /**
   * Output MessageProperties key to Std.out with parameters.
   */
  void writeMessage(String messageKey, String... args) {
    write(getMessage(messageKey, args));
  }

  /**
   * Output MessageProperties key to Std.out
   */
  void writeMessage(String messageKey) {
    outputWriter.write(getMessage(messageKey));
  }

  /**
   * Add a slight blank space above new screen.
   *
   * TODO-Can replace by cls command.
   */
  private void outputScreenScroll() {
    write("\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n");
  }

  /**
   * Prints a separator line between each section of screen.
   */
  void outputSeparator() {
    outputWriter.separator();
  }

  /**
   * Outputs Header Message
   */
  void outputHeader(GameSession gameSession) {
    String header = getMessage("game.name", getHeaderMessage());
    if (gameSession != null && gameSession.getPlayer() != null
        && gameSession.getPlayer().getName() != null) {
      write(
          appendAtLocation(header, HEADER_PLAYER_PREFIX + gameSession.getPlayer().getName(), 50));
    } else {
      write(appendAtLocation(header, HEADER_PLAYER_GUEST, 50));
    }
  }

  /**
   * Utility methos to output formatter Player stats
   */
  void outputPlayerStats(Player player) {
    writeMessage("player.stats.heading");
    outputSeparator();

    write(appendAtLocation(getMessage("player.stats.name"), player.getName(),
        PLAYER_STATS_SEPARATION));
    write(appendAtLocation(getMessage("player.stats.health"),
        formattedHealth(player), PLAYER_STATS_SEPARATION));
    write(appendAtLocation(getMessage("player.stats.experience"),
        valueOf(player.getExperience()), PLAYER_STATS_SEPARATION));

    if (!player.getAttackWeapons().isEmpty()) {
      writeMessage("player.stats.attackweapons.header");
      player.getAttackWeapons().forEach(attackWeapon -> {
        writeMessage("player.stats.weapon", attackWeapon.getName(),
            valueOf(attackWeapon.getMinPower()),
            valueOf(attackWeapon.getMaxPower()));
      });
    }

    if (!player.getDefenseWeapons().isEmpty()) {
      writeMessage("player.stats.defenseweapons.header");
      player.getDefenseWeapons().forEach(defenseWeapon -> {
        writeMessage("player.stats.weapon", defenseWeapon.getName(),
            valueOf(defenseWeapon.getMinPower()),
            valueOf(defenseWeapon.getMaxPower()));
      });
    }
  }

  /**
   * Pause screen for random input.
   */
  void moveOnEnter(GameSession session, GameScreen nextScreen) {
    writeMessage("generic.pressenter.tocontinue");
    inputReader.read();
    nextScreen.render(session);
  }

  /**
   * Main controller method that is invoked to render the screen.
   */
  public void render(GameSession gameSession) {
    if (gameSession != null && storeCurrentScreen()) {
      gameSession.setCurrentScreen(this.getClass());
    }

    //Header region
    outputScreenScroll();
    outputSeparator();
    outputHeader(gameSession);
    outputSeparator();
    write("");

    //body region
    outputBody(gameSession);
  }

  /**
   * to be implemented for individual screens.
   */
  protected abstract void outputBody(GameSession session);

  /**
   * Get header message section for each screen.
   */
  protected abstract String getHeaderMessage();

}
