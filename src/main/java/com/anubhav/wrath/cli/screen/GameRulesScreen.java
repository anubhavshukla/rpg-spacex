package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.di.CliBeanFactory;
import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Implementation of screen for showing Game Rules and Instructions.
 */
public class GameRulesScreen extends GameScreen {

  private WelcomeScreen welcomeScreen;

  public GameRulesScreen(OutputWriter outputWriter, InputReader inputReader,
      WelcomeScreen welcomeScreen) {
    super(outputWriter, inputReader);
    this.welcomeScreen = welcomeScreen;
  }

  @Override
  boolean storeCurrentScreen() {
    return false;
  }

  @Override
  protected void outputBody(GameSession session) {
    writeMessage("screen.gamerules.details");
    outputNewLine();
    outputSeparator();

    writeMessage("screen.gamerules.return.message");
    inputReader.read();

    renderNextScreen(session);
  }

  private void renderNextScreen(GameSession session) {
    if (session != null && session.getCurrentScreen() != null) {
      CliBeanFactory.getBean(session.getCurrentScreen()).render(session);
    } else {
      welcomeScreen.render(session);
    }
  }

  @Override
  protected String getHeaderMessage() {
    return MessageResource.getMessage("screen.gamerules.header");
  }
}
