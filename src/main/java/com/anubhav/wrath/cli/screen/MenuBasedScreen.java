package com.anubhav.wrath.cli.screen;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.cli.menu.Menu;
import com.anubhav.wrath.cli.menu.MenuItem;
import com.anubhav.wrath.cli.store.MenuStore;
import com.anubhav.wrath.core.exception.GameException;
import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;
import java.util.Arrays;
import java.util.Optional;

/**
 * Abstract implementation of screen requiring menu options.
 */
public abstract class MenuBasedScreen extends GameScreen {

  private static final String MENU_ITEM_SEPARATOR = MessageResource
      .getMessage("menu.item.separator");

  MenuBasedScreen(OutputWriter outputWriter,
      InputReader inputReader) {
    super(outputWriter, inputReader);
  }

  /**
   * Readed user selection.
   */
  String readMenuInput() {
    String shortCode;
    do {
      shortCode = inputReader.read();
    } while (isInvalidSelection(shortCode));
    return shortCode;
  }

  /**
   * Verify if correct selection entry was made.
   */
  private boolean isInvalidSelection(String shortCode) {
    boolean isValidSelection = Arrays.stream(getMenu().getMenuItems())
        .anyMatch(menuItem -> menuItem.getShortCode().equals(shortCode));
    if (!isValidSelection) {
      writeMessage("menu.selection.invalid");
    }
    return !isValidSelection;
  }

  /**
   * Render menu to OutputWriter
   */
  void outputMenu() {
    Optional<Menu> menuOptional = MenuStore.get(getMenuId());
    if (menuOptional.isPresent()) {
      Menu menu = menuOptional.get();
      write(menu.getGreeting());
      StringBuilder menuItemStr = new StringBuilder();
      for (MenuItem menuItem : menu.getMenuItems()) {
        menuItemStr.append("(").append(menuItem.getShortCode()).append(") ")
            .append(menuItem.getMessage()).append(" ").append(MENU_ITEM_SEPARATOR).append(" ");
      }
      write(menuItemStr.substring(0, menuItemStr.lastIndexOf(MENU_ITEM_SEPARATOR)));
      writeMessage("menu.selection.message");
    }
  }

  /**
   * Get MenuItem based on selection made by user.
   */
  MenuItem findMenuItem(String shortCode) {
    return Arrays
        .stream(getMenu().getMenuItems())
        .filter(menuItem -> menuItem.getShortCode().equals(shortCode))
        .findFirst()
        .orElseThrow(GameException::new);
  }

  /**
   * Get menu object from store.
   */
  private Menu getMenu() {
    return MenuStore.get(getMenuId()).orElseThrow(NotFoundException::new);
  }

  /**
   * Remder the screen for MenuBased actions.
   */
  public void render(GameSession gameSession) {
    super.render(gameSession);
    outputNewLine();
    outputSeparator();
    outputMenu();
    String selection = readMenuInput();
    findMenuItem(selection).getListener().execute(gameSession);
  }

  /**
   * Individual implementation will provide the menu-id to be rendered.
   */
  protected abstract String getMenuId();

}
