package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.cli.constant.CliConstant.FIGHT_WON_MENU_ID;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.exception.InvalidStateException;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Implementation of screen for scenario where Player has won the fight.
 */
public class FightWonScreen extends MenuBasedScreen {

  public FightWonScreen(OutputWriter outputWriter,
      InputReader inputReader) {
    super(outputWriter, inputReader);
  }

  @Override
  protected String getMenuId() {
    return FIGHT_WON_MENU_ID;
  }

  @Override
  protected void outputBody(GameSession session) {
    if (session == null || session.getPlayer() == null) {
      throw new InvalidStateException(
          MessageResource.getMessage("generic.invalid.session"));
    }

    if (FightStatusEnum.WON != session.getFightStatus()) {
      throw new InvalidStateException(MessageResource.getMessage("screen.fightwon.notwon"));
    }

    writeMessage("screen.fightwon.congrats", session.getActiveEnemy().getName());

    writeMessage("screen.fightwon.message2");

    outputPlayerStats(session.getPlayer());
  }


  @Override
  protected String getHeaderMessage() {
    return MessageResource.getMessage("screen.fightwon.header");
  }
}
