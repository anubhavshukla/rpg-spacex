package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.cli.constant.CliConstant.PLAYER_MENU_ID;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.i18n.MessageResource;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Start a new game.
 */
public class StartNewGameScreen extends MenuBasedScreen {

  public StartNewGameScreen(OutputWriter outputWriter,
      InputReader inputReader) {
    super(outputWriter, inputReader);
  }

  public String getMenuId() {
    return PLAYER_MENU_ID;
  }

  public String getHeaderMessage() {
    return MessageResource.getMessage("screen.startnewgame.header");
  }

  public void outputBody(GameSession session) {
    writeMessage("screen.startnewgame.message");
  }
}
