package com.anubhav.wrath.cli.screen;

import static com.anubhav.wrath.core.i18n.MessageResource.getMessage;

import com.anubhav.wrath.cli.io.InputReader;
import com.anubhav.wrath.cli.io.OutputWriter;
import com.anubhav.wrath.core.exception.InvalidStateException;
import com.anubhav.wrath.core.model.GameStatusEnum;
import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Implementation of screen for scenario where Player has defeated all Enemies and won the game.
 */
public class GameWonScreen extends GameScreen {

  private WelcomeScreen welcomeScreen;

  public GameWonScreen(OutputWriter outputWriter, InputReader inputReader,
      WelcomeScreen welcomeScreen) {
    super(outputWriter, inputReader);
    this.welcomeScreen = welcomeScreen;
  }

  @Override
  protected void outputBody(GameSession session) {
    if (session == null || session.getPlayer() == null) {
      throw new InvalidStateException(getMessage("generic.invalid.session"));
    }

    if (GameStatusEnum.COMPLETED != session.getGameStatus()) {
      throw new InvalidStateException(getMessage("screen.gamewon.invalid.state"));
    }

    writeMessage("screen.gamewon.message1");

    outputPlayerStats(session.getPlayer());
    outputSeparator();

    moveOnEnter(null, welcomeScreen);
  }

  @Override
  protected String getHeaderMessage() {
    return getMessage("screen.gamewon.header");
  }
}
