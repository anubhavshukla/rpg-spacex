package com.anubhav.wrath.cli.utils;

import static com.anubhav.wrath.core.config.ApplicationProperties.get;
import static java.lang.Integer.parseInt;

import com.anubhav.wrath.core.config.ApplicationPropertyKey;

public class StringUtils {

  private static final int DEFAULT_COLUMN_WIDTH = parseInt(
      get(ApplicationPropertyKey.DEFAULT_COLUMN_WIDTH));

  private StringUtils() {
    //object instantiation not allowed
  }

  public static String appendAtLocation(String beginStr, String currentStr, int location) {
    StringBuilder stringBuilder = new StringBuilder(beginStr);
    for (int i = 0; i < (location - beginStr.length()); i++) {
      stringBuilder.append(" ");
    }
    return stringBuilder.append(currentStr).toString();
  }

  public static String appendAtLocation(String[] strings, int location) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < strings.length; i++) {
      stringBuilder.append(strings[i]);
      if (i != (strings.length - 1)) {
        for (int j = 0; j < (location - strings[i].length()); j++) {
          stringBuilder.append(" ");
        }
      }
    }
    return stringBuilder.toString();
  }

  public static String showInColumns(String... strings) {
    return appendAtLocation(strings, DEFAULT_COLUMN_WIDTH);
  }
}
