package com.anubhav.wrath.cli.utils;

import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.weapon.AttackWeapon;
import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.model.weapon.DefenseWeapon;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;

public class DisplayUtils {

  private DisplayUtils() {
    //object initialization not allowed
  }

  public static String formattedHealth(Player player) {
    return player.getHealth() + "/" + player.getMaxHealth();
  }

  public static String formattedHealth(Enemy enemy) {
    return enemy.getHealth() + "/" + enemy.getMaxHealth();
  }

  public static String weaponWithPower(AttackWeapon attackWeapon) {
    return attackWeapon.getName() + " (" + attackWeapon.getMinPower() + "-" + attackWeapon
        .getMaxPower() + ")";
  }

  public static String weaponWithPower(AttackWeaponTemplate attackWeapon) {
    return attackWeapon.getName() + " (" + attackWeapon.getMinPower() + "-" + attackWeapon
        .getMaxPower() + ")";
  }

  public static String weaponWithPower(DefenseWeapon defenseWeapon) {
    return defenseWeapon.getName() + " (" + defenseWeapon.getMinPower() + "-"
        + defenseWeapon.getMaxPower() + ")";
  }

  public static String weaponWithPower(DefenseWeaponTemplate defenseWeapon) {
    return defenseWeapon.getName() + " (" + defenseWeapon.getMinPower() + "-"
        + defenseWeapon.getMaxPower() + ")";
  }
}
