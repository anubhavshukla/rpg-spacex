package com.anubhav.wrath.core.repository.impl;

import com.anubhav.wrath.core.exception.NullValueException;
import com.anubhav.wrath.core.model.AbstractDataObject;
import com.anubhav.wrath.core.repository.CrudRepository;
import com.anubhav.wrath.core.util.CommonUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * In-memory implementation of common repository functions.
 *
 * @param <T> - Identifier type
 * @param <U> - Data object type
 */
public class InMemoryRepositoryImpl<T, U extends AbstractDataObject> implements
    CrudRepository<T, U> {

  protected Map<T, U> store = new LinkedHashMap<>();

  public Optional<U> find(T id) {
    return Optional.ofNullable(store.get(id));
  }

  public U save(U u) {
    if (u == null) {
      throw new NullValueException("Can not save null player.");
    }

    if (u.getId() == null) {
      u.setId(CommonUtils.generateId());
    }
    store.put((T) u.getId(), u);
    return store.get(u.getId());
  }

  public List<U> findAll() {
    return new ArrayList<>(store.values());
  }

  public U delete(T t) {
    return store.remove(t);
  }
}
