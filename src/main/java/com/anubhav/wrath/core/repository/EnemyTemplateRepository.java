package com.anubhav.wrath.core.repository;

import com.anubhav.wrath.core.model.character.EnemyTemplate;
import java.util.Optional;

public interface EnemyTemplateRepository extends CrudRepository<String, EnemyTemplate> {

  Optional<EnemyTemplate> findBySelectionOrder(int selectionOrder);
}
