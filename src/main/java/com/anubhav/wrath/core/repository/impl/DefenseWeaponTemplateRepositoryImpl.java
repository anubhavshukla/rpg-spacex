package com.anubhav.wrath.core.repository.impl;

import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;
import com.anubhav.wrath.core.repository.DefenseWeaponTemplateRepository;

public class DefenseWeaponTemplateRepositoryImpl
    extends InMemoryRepositoryImpl<String, DefenseWeaponTemplate>
    implements DefenseWeaponTemplateRepository {

}
