package com.anubhav.wrath.core.repository;

import com.anubhav.wrath.core.model.environment.GameSession;

public interface GameSessionRepository extends CrudRepository<String, GameSession> {

}
