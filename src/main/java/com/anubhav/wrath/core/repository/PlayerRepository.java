package com.anubhav.wrath.core.repository;

import com.anubhav.wrath.core.model.character.Player;

public interface PlayerRepository extends CrudRepository<String, Player> {

}
