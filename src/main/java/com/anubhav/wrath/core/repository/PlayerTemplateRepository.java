package com.anubhav.wrath.core.repository;

import com.anubhav.wrath.core.model.character.PlayerTemplate;

public interface PlayerTemplateRepository extends CrudRepository<String, PlayerTemplate> {

}
