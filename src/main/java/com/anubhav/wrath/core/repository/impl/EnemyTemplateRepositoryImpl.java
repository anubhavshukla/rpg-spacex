package com.anubhav.wrath.core.repository.impl;

import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.repository.EnemyTemplateRepository;
import java.util.Optional;

public class EnemyTemplateRepositoryImpl
    extends InMemoryRepositoryImpl<String, EnemyTemplate>
    implements EnemyTemplateRepository {

  @Override
  public Optional<EnemyTemplate> findBySelectionOrder(int selectionOrder) {
    return store.values().stream().filter(enemyTemplate -> matchSelectionOrder(selectionOrder,
        enemyTemplate)).findFirst();
  }

  private boolean matchSelectionOrder(int selectionOrder, EnemyTemplate enemyTemplate) {
    return enemyTemplate.getSelectionOrder() == selectionOrder;
  }
}
