package com.anubhav.wrath.core.repository.impl;

import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.repository.GameSessionRepository;

public class GameSessionRepositoryImpl
    extends InMemoryRepositoryImpl<String, GameSession>
    implements GameSessionRepository {

}
