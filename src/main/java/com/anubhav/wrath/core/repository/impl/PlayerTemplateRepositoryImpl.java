package com.anubhav.wrath.core.repository.impl;

import com.anubhav.wrath.core.model.character.PlayerTemplate;
import com.anubhav.wrath.core.repository.PlayerTemplateRepository;

public class PlayerTemplateRepositoryImpl
    extends InMemoryRepositoryImpl<String, PlayerTemplate>
    implements PlayerTemplateRepository {

}
