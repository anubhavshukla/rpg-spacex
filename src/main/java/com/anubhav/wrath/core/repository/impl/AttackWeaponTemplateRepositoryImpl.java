package com.anubhav.wrath.core.repository.impl;

import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.repository.AttackWeaponTemplateRepository;

public class AttackWeaponTemplateRepositoryImpl
    extends InMemoryRepositoryImpl<String, AttackWeaponTemplate>
    implements AttackWeaponTemplateRepository {

}
