package com.anubhav.wrath.core.repository;

import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;

public interface DefenseWeaponTemplateRepository extends
    CrudRepository<String, DefenseWeaponTemplate> {

}
