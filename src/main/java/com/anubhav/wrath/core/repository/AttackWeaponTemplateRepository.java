package com.anubhav.wrath.core.repository;

import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;

public interface AttackWeaponTemplateRepository extends
    CrudRepository<String, AttackWeaponTemplate> {

}
