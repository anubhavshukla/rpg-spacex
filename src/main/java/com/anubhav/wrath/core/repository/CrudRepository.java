package com.anubhav.wrath.core.repository;

import com.anubhav.wrath.core.model.AbstractDataObject;
import java.util.List;
import java.util.Optional;

/**
 * Generic definition of a CRUD repository.
 *
 * TODO- Can be replaced by Spring Repositories.
 */
public interface CrudRepository<T, U extends AbstractDataObject> {

  Optional<U> find(T id);

  U save(U u);

  List<U> findAll();

  U delete(T t);
}
