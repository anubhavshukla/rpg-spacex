package com.anubhav.wrath.core.repository.impl;

import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.repository.PlayerRepository;

/**
 * In-memory implementation of PlayerRepository
 */
public class PlayerRepositoryImpl extends InMemoryRepositoryImpl<String, Player> implements
    PlayerRepository {

}
