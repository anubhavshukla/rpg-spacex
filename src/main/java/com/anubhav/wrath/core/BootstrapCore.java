package com.anubhav.wrath.core;

import static com.anubhav.wrath.core.di.BeanFactory.getBean;

import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.PlayerTemplate;
import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;
import com.anubhav.wrath.core.repository.AttackWeaponTemplateRepository;
import com.anubhav.wrath.core.repository.DefenseWeaponTemplateRepository;
import com.anubhav.wrath.core.repository.EnemyTemplateRepository;
import com.anubhav.wrath.core.repository.PlayerTemplateRepository;

/**
 * Programmatic implementation of bootstrap class for a Game.
 *
 * TODO - Values can be externalized and loaded through csv,xml,json etc.
 */
public class BootstrapCore {

  private static AttackWeaponTemplateRepository attackWeaponTemplateRepository =
      getBean(AttackWeaponTemplateRepository.class);
  private static DefenseWeaponTemplateRepository defenseWeaponTemplateRepository =
      getBean(DefenseWeaponTemplateRepository.class);
  private static EnemyTemplateRepository enemyTemplateRepository =
      getBean(EnemyTemplateRepository.class);
  private static PlayerTemplateRepository playerTemplateRepository =
      getBean(PlayerTemplateRepository.class);

  public static void loadData() {
    createEnemyOne();
    createEnemyTwo();
    createPlayerTemplate();
  }

  private static void createPlayerTemplate() {
    AttackWeaponTemplate attackWeaponTemplate = new AttackWeaponTemplate();
    attackWeaponTemplate.setId("player-aw-0001");
    attackWeaponTemplate.setName("Dagger");
    attackWeaponTemplate.setMaxPower(20);
    attackWeaponTemplate.setMinPower(16);
    attackWeaponTemplateRepository.save(attackWeaponTemplate);

    DefenseWeaponTemplate defenseWeaponTemplate = new DefenseWeaponTemplate();
    defenseWeaponTemplate.setId("player-dw-0001");
    defenseWeaponTemplate.setName("Shield");
    defenseWeaponTemplate.setMaxPower(6);
    defenseWeaponTemplate.setMinPower(2);
    defenseWeaponTemplateRepository.save(defenseWeaponTemplate);

    PlayerTemplate playerTemplate = new PlayerTemplate();
    playerTemplate.setPrimaryAttackWeapon(attackWeaponTemplate);
    playerTemplate.setPrimaryDefenseWeapon(defenseWeaponTemplate);
    playerTemplate.setId("player-temp-0001");
    playerTemplate.setHealth(100);
    playerTemplateRepository.save(playerTemplate);
  }

  private static void createEnemyOne() {
    AttackWeaponTemplate attackWeaponTemplate = new AttackWeaponTemplate();
    attackWeaponTemplate.setId("e1-aw-0001");
    attackWeaponTemplate.setName("Axe");
    attackWeaponTemplate.setMaxPower(16);
    attackWeaponTemplate.setMinPower(12);
    attackWeaponTemplateRepository.save(attackWeaponTemplate);

    DefenseWeaponTemplate defenseWeaponTemplate = new DefenseWeaponTemplate();
    defenseWeaponTemplate.setId("e1-dw-0001");
    defenseWeaponTemplate.setName("Shield");
    defenseWeaponTemplate.setMaxPower(4);
    defenseWeaponTemplate.setMinPower(2);
    defenseWeaponTemplateRepository.save(defenseWeaponTemplate);

    AttackWeaponTemplate attackWeaponToWin = new AttackWeaponTemplate();
    attackWeaponToWin.setId("e1-aw-win-0001");
    attackWeaponToWin.setName("Sword");
    attackWeaponToWin.setMaxPower(30);
    attackWeaponToWin.setMinPower(22);
    attackWeaponTemplateRepository.save(attackWeaponToWin);

    DefenseWeaponTemplate defenseWeaponToWin = new DefenseWeaponTemplate();
    defenseWeaponToWin.setId("e1-dw-win-0001");
    defenseWeaponToWin.setName("Power Shield");
    defenseWeaponToWin.setMaxPower(7);
    defenseWeaponToWin.setMinPower(6);
    defenseWeaponTemplateRepository.save(defenseWeaponToWin);

    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setAttackWeapon(attackWeaponTemplate);
    enemyTemplate.setDefenseWeapon(defenseWeaponToWin);
    enemyTemplate.addAttackWeaponToEarn(attackWeaponToWin);
    enemyTemplate.addDefenseWeaponToEarn(defenseWeaponToWin);
    enemyTemplate.setExperiencePointsToEarn(500);
    enemyTemplate.setHealth(100);
    enemyTemplate.setName("Gugornought");
    enemyTemplate.setId("gugornought");
    enemyTemplate.setSelectionOrder(0);
    enemyTemplateRepository.save(enemyTemplate);
  }

  private static void createEnemyTwo() {
    AttackWeaponTemplate attackWeaponTemplate = new AttackWeaponTemplate();
    attackWeaponTemplate.setId("e2-aw-0001");
    attackWeaponTemplate.setName("Double Side Axe");
    attackWeaponTemplate.setMaxPower(25);
    attackWeaponTemplate.setMinPower(20);
    attackWeaponTemplateRepository.save(attackWeaponTemplate);

    DefenseWeaponTemplate defenseWeaponTemplate = new DefenseWeaponTemplate();
    defenseWeaponTemplate.setId("e2-dw-0001");
    defenseWeaponTemplate.setName("Photon Shield");
    defenseWeaponTemplate.setMaxPower(10);
    defenseWeaponTemplate.setMinPower(8);
    defenseWeaponTemplateRepository.save(defenseWeaponTemplate);

    AttackWeaponTemplate attackWeaponToWin = new AttackWeaponTemplate();
    attackWeaponToWin.setId("e2-aw-win-0001");
    attackWeaponToWin.setName("Light Saber");
    attackWeaponToWin.setMaxPower(40);
    attackWeaponToWin.setMinPower(35);
    attackWeaponTemplateRepository.save(attackWeaponToWin);

    DefenseWeaponTemplate defenseWeaponToWin = new DefenseWeaponTemplate();
    defenseWeaponToWin.setId("e2-dw-win-0002");
    defenseWeaponToWin.setName("Ultra Shield");
    defenseWeaponToWin.setMaxPower(10);
    defenseWeaponToWin.setMinPower(8);
    defenseWeaponTemplateRepository.save(defenseWeaponToWin);

    EnemyTemplate enemyTemplate = new EnemyTemplate();
    enemyTemplate.setAttackWeapon(attackWeaponTemplate);
    enemyTemplate.setDefenseWeapon(defenseWeaponToWin);
    enemyTemplate.addAttackWeaponToEarn(attackWeaponToWin);
    enemyTemplate.addDefenseWeaponToEarn(defenseWeaponToWin);
    enemyTemplate.setExperiencePointsToEarn(500);
    enemyTemplate.setHealth(100);
    enemyTemplate.setName("Ninja");
    enemyTemplate.setId("ninja");
    enemyTemplate.setSelectionOrder(1);
    enemyTemplateRepository.save(enemyTemplate);
  }
}
