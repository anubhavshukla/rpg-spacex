package com.anubhav.wrath.core.exception;

/**
 * Exception caused if unable to load bean through dependency injection.
 */
public class DIException extends GameException {

  public DIException() {
  }

  public DIException(String message) {
    super(message);
  }
}
