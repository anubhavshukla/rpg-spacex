package com.anubhav.wrath.core.exception;

/**
 * Exception thrown if request value is not found.
 */
public class NotFoundException extends GameException {

  public NotFoundException() {
  }

  public NotFoundException(String message) {
    super(message);
  }
}
