package com.anubhav.wrath.core.exception;

/**
 * Super class of all exceptions in this application.
 */
public class GameException extends RuntimeException {

  public GameException() {
  }

  public GameException(String message) {
    super(message);
  }
}
