package com.anubhav.wrath.core.exception;

/**
 * Exception thrown if current state of objects is not correct for processing.
 */
public class InvalidStateException extends GameException {

  public InvalidStateException() {
  }

  public InvalidStateException(String message) {
    super(message);
  }
}
