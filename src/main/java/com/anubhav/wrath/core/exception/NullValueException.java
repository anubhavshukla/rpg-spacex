package com.anubhav.wrath.core.exception;

/**
 * Exception through if mandatory value does not exist.
 */
public class NullValueException extends GameException {

  public NullValueException() {
  }

  public NullValueException(String message) {
    super(message);
  }
}
