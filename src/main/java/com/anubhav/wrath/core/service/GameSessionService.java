package com.anubhav.wrath.core.service;

import com.anubhav.wrath.core.model.environment.GameSession;
import java.util.List;

/**
 * APIs for managing a GameSession.
 */
public interface GameSessionService {

  GameSession startNew();

  GameSession save(GameSession session);

  List<GameSession> findAll();

  GameSession delete(GameSession gameSession);
}
