package com.anubhav.wrath.core.service;

import com.anubhav.wrath.core.model.character.Player;

/**
 * APIs for managing Player object.
 */
public interface PlayerService {

  Player create(String name);

  Player setActiveAttackWeapon(Player player, String attackWeaponId);

  Player setActiveDefenseWeapon(Player player, String defenseWeaponId);
}
