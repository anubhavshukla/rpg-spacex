package com.anubhav.wrath.core.service;

import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * APIs for managing the Fight.
 */
public interface FightService {

  GameSession executeAttack(GameSession gameSession);

  GameSession startFight(GameSession gameSession);
}
