package com.anubhav.wrath.core.service.impl;

import static com.anubhav.wrath.core.config.ApplicationPropertyKey.PLAYER_TEMPLATE_ID;

import com.anubhav.wrath.core.config.ApplicationProperties;
import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.character.PlayerTemplate;
import com.anubhav.wrath.core.model.weapon.AttackWeapon;
import com.anubhav.wrath.core.model.weapon.DefenseWeapon;
import com.anubhav.wrath.core.repository.PlayerTemplateRepository;
import com.anubhav.wrath.core.service.PlayerService;

/**
 * Implementation of PlayerService
 */
public class PlayerServiceImpl implements PlayerService {

  private PlayerTemplateRepository playerTemplateRepository;

  public PlayerServiceImpl(PlayerTemplateRepository playerTemplateRepository) {
    this.playerTemplateRepository = playerTemplateRepository;
  }

  @Override
  public Player create(String name) {
    PlayerTemplate playerTemplate = playerTemplateRepository
        .find(ApplicationProperties.get(PLAYER_TEMPLATE_ID)).orElseThrow(NotFoundException::new);

    Player player = new Player(playerTemplate);
    player.setName(name);
    return player;
  }

  @Override
  public Player setActiveAttackWeapon(Player player, String attackWeaponId) {
    AttackWeapon attackWeapon = player.getAttackWeapons().stream()
        .filter(weapon -> weapon.getTemplate().getId().equals(attackWeaponId))
        .findFirst()
        .orElseThrow(NotFoundException::new);
    player.setActiveAttackWeapon(attackWeapon);
    return player;
  }

  @Override
  public Player setActiveDefenseWeapon(Player player, String defenseWeaponId) {
    DefenseWeapon defenseWeapon = player.getDefenseWeapons().stream()
        .filter(weapon -> weapon.getTemplate().getId().equals(defenseWeaponId))
        .findFirst()
        .orElseThrow(NotFoundException::new);
    player.setActiveDefenseWeapon(defenseWeapon);
    return player;
  }
}
