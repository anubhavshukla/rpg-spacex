package com.anubhav.wrath.core.service.impl;

import com.anubhav.wrath.core.calculator.DamageCalculator;
import com.anubhav.wrath.core.calculator.ExperiencePointCalculator;
import com.anubhav.wrath.core.exception.NotFoundException;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.GameStatusEnum;
import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.EnemyTemplate;
import com.anubhav.wrath.core.model.character.Player;
import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.model.weapon.AttackWeapon;
import com.anubhav.wrath.core.model.weapon.DefenseWeapon;
import com.anubhav.wrath.core.repository.EnemyTemplateRepository;
import com.anubhav.wrath.core.service.FightService;
import com.anubhav.wrath.core.service.GameSessionService;

/**
 * Implementation of FightService.
 */
public class FightServiceImpl implements FightService {

  private DamageCalculator damageCalculator;

  private ExperiencePointCalculator experiencePointCalculator;

  private EnemyTemplateRepository enemyTemplateRepository;

  private GameSessionService gameSessionService;

  public FightServiceImpl(DamageCalculator damageCalculator,
      ExperiencePointCalculator experiencePointCalculator,
      EnemyTemplateRepository enemyTemplateRepository,
      GameSessionService gameSessionService) {
    this.damageCalculator = damageCalculator;
    this.experiencePointCalculator = experiencePointCalculator;
    this.enemyTemplateRepository = enemyTemplateRepository;
    this.gameSessionService = gameSessionService;
  }

  @Override
  public GameSession executeAttack(GameSession gameSession) {
    Player player = gameSession.getPlayer();
    Enemy enemy = gameSession.getActiveEnemy();

    player.setHealth(calculatePlayerHealth(player, enemy));
    enemy.setHealth(calculateEnemyHealth(enemy, player));

    if (enemy.getHealth() == 0) {
      handlePlayerWon(gameSession);
    } else if (player.getHealth() == 0) {
      handlePlayerLost(gameSession);
    }
    return gameSession;
  }

  @Override
  public GameSession startFight(GameSession gameSession) {

    gameSession.setFightStatus(FightStatusEnum.IN_PROGRESS);
    setupPlayer(gameSession);
    setupActiveEnemy(gameSession);

    return gameSession;
  }

  private void setupActiveEnemy(GameSession gameSession) {
    if (gameSession.getActiveEnemy() == null) {
      gameSession.setActiveEnemy(getFirstEnemy());
    } else {
      gameSession.setActiveEnemy(getNextEnemy(gameSession.getActiveEnemy()));
    }
  }

  private Enemy getNextEnemy(Enemy enemy) {
    EnemyTemplate enemyTemplate = enemyTemplateRepository
        .findBySelectionOrder(enemy.getSelectionOrder() + 1).orElseThrow(NotFoundException::new);
    return new Enemy(enemyTemplate);
  }

  private Enemy getFirstEnemy() {
    EnemyTemplate enemyTemplate = enemyTemplateRepository.findBySelectionOrder(0).orElseThrow(
        NotFoundException::new);
    return new Enemy(enemyTemplate);
  }

  private void setupPlayer(GameSession gameSession) {
    Player player = gameSession.getPlayer();
    if (player.getActiveAttackWeapon() == null) {
      player.setActiveAttackWeapon(
          new AttackWeapon(player.getTemplate().getPrimaryAttackWeapon()));
    }

    if (player.getActiveDefenseWeapon() == null) {
      player.setActiveDefenseWeapon(
          new DefenseWeapon(player.getTemplate().getPrimaryDefenseWeapon()));
    }
  }

  private void handlePlayerWon(GameSession gameSession) {
    gameSession.setFightStatus(FightStatusEnum.WON);

    Player player = gameSession.getPlayer();
    player.setHealth(player.getTemplate().getHealth());
    player.setExperience(experiencePointCalculator.calculate(gameSession));

    Enemy enemy = gameSession.getActiveEnemy();
    enemy.getAttackWeaponsToEarn().forEach(
        attackWeaponTemplate -> player.addAttackWeapon(new AttackWeapon(attackWeaponTemplate)));
    enemy.getDefenseWeaponsToEarn().forEach(
        defenseWeaponTemplate -> player.addDefenseWeapon(new DefenseWeapon(defenseWeaponTemplate)));

    if (isLastEnemy(enemy)) {
      gameSession.setGameStatus(GameStatusEnum.COMPLETED);
      gameSessionService.delete(gameSession);
    }
  }

  private boolean isLastEnemy(Enemy enemy) {
    return !enemyTemplateRepository.findBySelectionOrder(enemy.getSelectionOrder() + 1).isPresent();
  }

  private void handlePlayerLost(GameSession gameSession) {
    gameSession.setFightStatus(FightStatusEnum.LOST);
    gameSessionService.delete(gameSession);
  }

  private int calculatePlayerHealth(Player player, Enemy enemy) {
    int playerHealth = player.getHealth() - damageCalculator.calculate(player, enemy);
    if (playerHealth < 0) {
      playerHealth = 0;
    }
    return playerHealth;
  }

  private int calculateEnemyHealth(Enemy enemy, Player player) {
    int remainingHealth = enemy.getHealth() - damageCalculator.calculate(enemy, player);
    if (remainingHealth < 0) {
      remainingHealth = 0;
    }
    return remainingHealth;
  }
}
