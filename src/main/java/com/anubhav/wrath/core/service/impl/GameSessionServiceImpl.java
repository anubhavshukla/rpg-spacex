package com.anubhav.wrath.core.service.impl;

import com.anubhav.wrath.core.model.environment.GameSession;
import com.anubhav.wrath.core.repository.GameSessionRepository;
import com.anubhav.wrath.core.service.GameSessionService;
import java.util.Calendar;
import java.util.List;

/**
 * Implementation of GameSessionService.
 */
public class GameSessionServiceImpl implements GameSessionService {

  private GameSessionRepository gameSessionRepository;

  public GameSessionServiceImpl(GameSessionRepository gameSessionRepository) {
    this.gameSessionRepository = gameSessionRepository;
  }

  @Override
  public GameSession startNew() {
    return new GameSession();
  }

  @Override
  public GameSession save(GameSession session) {
    session.setName(Calendar.getInstance().getTime().toString());
    return gameSessionRepository.save(session);
  }

  @Override
  public List<GameSession> findAll() {
    return gameSessionRepository.findAll();
  }

  @Override
  public GameSession delete(GameSession gameSession) {
    return gameSessionRepository.delete(gameSession.getId());
  }
}
