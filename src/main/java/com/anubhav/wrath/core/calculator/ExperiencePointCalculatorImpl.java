package com.anubhav.wrath.core.calculator;

import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Simple implementation of experience point calculation logic.
 */
public class ExperiencePointCalculatorImpl implements ExperiencePointCalculator {

  @Override
  public int calculate(GameSession gameSession) {
    return gameSession.getPlayer().getExperience() +
        gameSession.getActiveEnemy().getExperiencePointsToEarn();
  }
}
