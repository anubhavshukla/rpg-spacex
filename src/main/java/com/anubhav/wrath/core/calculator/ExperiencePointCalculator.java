package com.anubhav.wrath.core.calculator;

import com.anubhav.wrath.core.model.environment.GameSession;

/**
 * Calculate experience points earned by Player after winning a fight
 */
public interface ExperiencePointCalculator {

  int calculate(GameSession gameSession);

}
