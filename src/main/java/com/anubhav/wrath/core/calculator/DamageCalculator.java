package com.anubhav.wrath.core.calculator;

import com.anubhav.wrath.core.model.character.Fighter;

/**
 * Calculate damage points on Defender by Attacker.
 */
public interface DamageCalculator {

  int calculate(Fighter defender, Fighter attacker);

}
