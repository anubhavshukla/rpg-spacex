package com.anubhav.wrath.core.calculator;

import com.anubhav.wrath.core.model.character.Fighter;
import java.util.Random;

/**
 * Simple implementation for DamageCalculator
 */
public class DamagePointCalculator implements DamageCalculator {

  private Random random = new Random();

  @Override
  public int calculate(Fighter defender, Fighter attacker) {

    int attackPoints = attacker.getMinAttackPoints() + random
        .nextInt(attacker.getMaxAttackPoints() - attacker.getMinAttackPoints());

    int defensePoints = defender.getMinDefensePoints() + random
        .nextInt(defender.getMaxDefensePoints() - defender.getMinDefensePoints());

    int damagePoints = attackPoints - defensePoints;

    return damagePoints > 0 ? damagePoints : 0;
  }
}
