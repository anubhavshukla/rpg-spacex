package com.anubhav.wrath.core.model.character;

/**
 * Definition of a character involved in fights.
 */
public interface Fighter {

  int getMaxAttackPoints();

  int getMinAttackPoints();

  int getMaxDefensePoints();

  int getMinDefensePoints();
}
