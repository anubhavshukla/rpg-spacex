package com.anubhav.wrath.core.model.weapon;

import com.anubhav.wrath.core.model.AbstractDataObject;

/**
 * Defines a generic WeaponTemplate in the system.
 */
public abstract class WeaponTemplate extends AbstractDataObject {

  /**
   * Name of weapon
   */
  private String name;

  /**
   * Maximum damage/protection during attack.
   */
  private int minPower;

  /**
   * Minimum possible damage/protection during attack.
   */
  private int maxPower;

  public int getMinPower() {
    return minPower;
  }

  public void setMinPower(int minPower) {
    this.minPower = minPower;
  }

  public int getMaxPower() {
    return maxPower;
  }

  public void setMaxPower(int maxPower) {
    this.maxPower = maxPower;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
