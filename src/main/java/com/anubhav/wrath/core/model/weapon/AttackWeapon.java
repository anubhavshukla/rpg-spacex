package com.anubhav.wrath.core.model.weapon;

/**
 * Attack weapon used by Player and Enemy.
 */
public class AttackWeapon extends Weapon {

  /**
   * Template definition of this weapon.
   */
  private AttackWeaponTemplate template;

  public AttackWeapon() {
  }

  public AttackWeapon(AttackWeaponTemplate template) {
    this.template = template;
  }

  public AttackWeaponTemplate getTemplate() {
    return template;
  }

  public String getTemplateId() {
    return template.getId();
  }

  public String getName() {
    return template.getName();
  }

  public int getMaxPower() {
    return template.getMaxPower();
  }

  public int getMinPower() {
    return template.getMinPower();
  }
}
