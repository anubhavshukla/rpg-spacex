package com.anubhav.wrath.core.model.environment;

import com.anubhav.wrath.cli.screen.GameScreen;
import com.anubhav.wrath.core.model.AbstractDataObject;
import com.anubhav.wrath.core.model.FightStatusEnum;
import com.anubhav.wrath.core.model.GameStatusEnum;
import com.anubhav.wrath.core.model.character.Enemy;
import com.anubhav.wrath.core.model.character.Player;

/**
 * Describes one active session of the game. Can be stored and retrieved through repositories.
 */
public class GameSession extends AbstractDataObject {

  /**
   * Player for this session.
   */
  private Player player;

  /**
   * Name used during saving this session.
   */
  private String name;

  /**
   * Current active screen. Is used while loading saved game.
   */
  private Class<? extends GameScreen> currentScreen;

  /**
   * Current or last active enemy.
   */
  private Enemy activeEnemy;

  /**
   * Last or current fight status
   */
  private FightStatusEnum fightStatus = FightStatusEnum.NOT_IN_PROGRESS;

  /**
   * Current Game status
   */
  private GameStatusEnum gameStatus = GameStatusEnum.IN_PROGRESS;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Player getPlayer() {
    return player;
  }

  public void setPlayer(Player player) {
    this.player = player;
  }

  public Class<? extends GameScreen> getCurrentScreen() {
    return currentScreen;
  }

  public void setCurrentScreen(Class<? extends GameScreen> currentScreen) {
    this.currentScreen = currentScreen;
  }

  public Enemy getActiveEnemy() {
    return activeEnemy;
  }

  public void setActiveEnemy(Enemy activeEnemy) {
    this.activeEnemy = activeEnemy;
  }

  public FightStatusEnum getFightStatus() {
    return fightStatus;
  }

  public void setFightStatus(FightStatusEnum fightStatus) {
    this.fightStatus = fightStatus;
  }

  public GameStatusEnum getGameStatus() {
    return gameStatus;
  }

  public void setGameStatus(GameStatusEnum gameStatus) {
    this.gameStatus = gameStatus;
  }
}
