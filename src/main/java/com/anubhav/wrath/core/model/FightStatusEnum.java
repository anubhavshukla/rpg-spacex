package com.anubhav.wrath.core.model;

/**
 * Possible status of a Fight.
 */
public enum FightStatusEnum {

  NOT_IN_PROGRESS,
  IN_PROGRESS,
  WON,
  LOST

}
