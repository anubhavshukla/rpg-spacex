package com.anubhav.wrath.core.model.character;

import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;

/**
 * Pre-configured template of the Player in game.
 */
public class PlayerTemplate extends Character {

  /**
   * Default attack weapon of this Player
   */
  private AttackWeaponTemplate primaryAttackWeapon;

  /**
   * Default defense weapon of this Player.
   */
  private DefenseWeaponTemplate primaryDefenseWeapon;

  public AttackWeaponTemplate getPrimaryAttackWeapon() {
    return primaryAttackWeapon;
  }

  public void setPrimaryAttackWeapon(AttackWeaponTemplate primaryAttackWeapon) {
    this.primaryAttackWeapon = primaryAttackWeapon;
  }

  public DefenseWeaponTemplate getPrimaryDefenseWeapon() {
    return primaryDefenseWeapon;
  }

  public void setPrimaryDefenseWeapon(DefenseWeaponTemplate primaryDefenseWeapon) {
    this.primaryDefenseWeapon = primaryDefenseWeapon;
  }
}
