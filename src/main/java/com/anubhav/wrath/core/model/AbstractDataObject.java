package com.anubhav.wrath.core.model;

/**
 * An abstract data object that has Id and can be stored in repositories.
 */
public abstract class AbstractDataObject {

  /**
   * Object identifier
   */
  private String id;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
