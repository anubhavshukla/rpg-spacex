package com.anubhav.wrath.core.model.character;

import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;
import java.util.Collections;
import java.util.List;

/**
 * The adversary with whom the Player has to fight.
 */
public class Enemy extends Character implements Fighter {

  /**
   * Template for this enemy.
   */
  private EnemyTemplate template;

  public Enemy() {
  }

  public Enemy(EnemyTemplate template) {
    this.template = template;
    this.setHealth(template.getHealth());
  }

  public EnemyTemplate getTemplate() {
    return template;
  }

  @Override
  public String getName() {
    return template.getName();
  }

  public int getMaxAttackPoints() {
    return template.getAttackWeapon().getMaxPower();
  }

  public int getMinAttackPoints() {
    return template.getAttackWeapon().getMinPower();
  }

  public int getMaxDefensePoints() {
    return template.getDefenseWeapon().getMaxPower();
  }

  public int getMinDefensePoints() {
    return template.getDefenseWeapon().getMinPower();
  }

  public int getMaxHealth() {
    return template.getHealth();
  }

  public int getExperiencePointsToEarn() {
    return template.getExperiencePointsToEarn();
  }

  public List<AttackWeaponTemplate> getAttackWeaponsToEarn() {
    return template.getAttackWeaponsToEarn() != null ? template.getAttackWeaponsToEarn()
        : Collections.emptyList();
  }

  public List<DefenseWeaponTemplate> getDefenseWeaponsToEarn() {
    return template.getDefenseWeaponsToEarn() != null ? template.getDefenseWeaponsToEarn()
        : Collections.emptyList();
  }

  public int getSelectionOrder() {
    return template.getSelectionOrder();
  }
}
