package com.anubhav.wrath.core.model.character;

import com.anubhav.wrath.core.model.weapon.AttackWeaponTemplate;
import com.anubhav.wrath.core.model.weapon.DefenseWeaponTemplate;
import java.util.LinkedList;
import java.util.List;

/**
 * Template of an enemy.
 */
public class EnemyTemplate extends Character {

  /**
   * Order in which this enemy will be encountered. Starts from 0
   */
  private int selectionOrder;

  /**
   * Primary attack weapon of this enemy
   */
  private AttackWeaponTemplate attackWeapon;

  /**
   * Primary Defense weapon of this enemy.
   */
  private DefenseWeaponTemplate defenseWeapon;

  /**
   * Experience points gained by Player of defeating this Enemy.
   */
  private int experiencePointsToEarn;

  /**
   * Attack weapons gained by Player after defeating this Enemy
   */
  private List<AttackWeaponTemplate> attackWeaponsToEarn;

  /**
   * Defense weapons gained by Player after defeating this Enemy
   */
  private List<DefenseWeaponTemplate> defenseWeaponsToEarn;

  public int getSelectionOrder() {
    return selectionOrder;
  }

  public void setSelectionOrder(int selectionOrder) {
    this.selectionOrder = selectionOrder;
  }

  public AttackWeaponTemplate getAttackWeapon() {
    return attackWeapon;
  }

  public void setAttackWeapon(AttackWeaponTemplate attackWeapon) {
    this.attackWeapon = attackWeapon;
  }

  public DefenseWeaponTemplate getDefenseWeapon() {
    return defenseWeapon;
  }

  public void setDefenseWeapon(DefenseWeaponTemplate defenseWeapon) {
    this.defenseWeapon = defenseWeapon;
  }

  public int getExperiencePointsToEarn() {
    return experiencePointsToEarn;
  }

  public void setExperiencePointsToEarn(int experiencePointsToEarn) {
    this.experiencePointsToEarn = experiencePointsToEarn;
  }

  public List<AttackWeaponTemplate> getAttackWeaponsToEarn() {
    return attackWeaponsToEarn;
  }

  public void setAttackWeaponsToEarn(List<AttackWeaponTemplate> attackWeaponsToEarn) {
    this.attackWeaponsToEarn = attackWeaponsToEarn;
  }

  public List<DefenseWeaponTemplate> getDefenseWeaponsToEarn() {
    return defenseWeaponsToEarn;
  }

  public void setDefenseWeaponsToEarn(List<DefenseWeaponTemplate> defenseWeaponsToEarn) {
    this.defenseWeaponsToEarn = defenseWeaponsToEarn;
  }

  public void addAttackWeaponToEarn(AttackWeaponTemplate attackWeaponTemplate) {
    if (attackWeaponsToEarn == null) {
      attackWeaponsToEarn = new LinkedList<>();
    }
    attackWeaponsToEarn.add(attackWeaponTemplate);
  }

  public void addDefenseWeaponToEarn(DefenseWeaponTemplate defenseWeaponTemplate) {
    if (defenseWeaponsToEarn == null) {
      defenseWeaponsToEarn = new LinkedList<>();
    }
    defenseWeaponsToEarn.add(defenseWeaponTemplate);
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof EnemyTemplate && getId().equals(((EnemyTemplate) obj).getId());
  }
}
