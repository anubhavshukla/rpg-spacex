package com.anubhav.wrath.core.model.character;

import com.anubhav.wrath.core.model.weapon.AttackWeapon;
import com.anubhav.wrath.core.model.weapon.DefenseWeapon;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Player or user in the game.
 */
public class Player extends Character implements Fighter {

  /**
   * Template from which this Player is created.
   */
  private PlayerTemplate template;

  /**
   * Current experience level.
   */
  private int experience;

  /**
   * All available attack weapons
   */
  private List<AttackWeapon> attackWeapons;

  /**
   * All available defense weapons.
   */
  private List<DefenseWeapon> defenseWeapons;

  /**
   * Cuurently selected attack weapon, will be used in fight.
   */
  private AttackWeapon activeAttackWeapon;

  /**
   * Cuurently selected defense weapon, will be used in fight.
   */
  private DefenseWeapon activeDefenseWeapon;

  public Player() {
  }

  public Player(PlayerTemplate template) {
    this.template = template;
    if (template.getPrimaryAttackWeapon() != null) {
      addAttackWeapon(new AttackWeapon(template.getPrimaryAttackWeapon()));
    }

    if (template.getPrimaryDefenseWeapon() != null) {
      addDefenseWeapon(new DefenseWeapon(template.getPrimaryDefenseWeapon()));
    }
    this.setHealth(this.template.getHealth());
  }

  public int getExperience() {
    return experience;
  }

  public void setExperience(int experience) {
    this.experience = experience;
  }

  public List<AttackWeapon> getAttackWeapons() {
    return attackWeapons != null ? attackWeapons : Collections.emptyList();
  }

  public void addAttackWeapon(AttackWeapon attackWeapon) {
    if (attackWeapons == null) {
      attackWeapons = new LinkedList<>();
    }
    attackWeapons.add(attackWeapon);
  }

  public List<DefenseWeapon> getDefenseWeapons() {
    return defenseWeapons != null ? defenseWeapons : Collections.emptyList();
  }

  public void addDefenseWeapon(DefenseWeapon defenseWeapon) {
    if (defenseWeapons == null) {
      defenseWeapons = new LinkedList<>();
    }
    defenseWeapons.add(defenseWeapon);
  }

  public AttackWeapon getActiveAttackWeapon() {
    return activeAttackWeapon;
  }

  public void setActiveAttackWeapon(AttackWeapon activeAttackWeapon) {
    this.activeAttackWeapon = activeAttackWeapon;
  }

  public DefenseWeapon getActiveDefenseWeapon() {
    return activeDefenseWeapon;
  }

  public void setActiveDefenseWeapon(DefenseWeapon activeDefenseWeapon) {
    this.activeDefenseWeapon = activeDefenseWeapon;
  }

  public PlayerTemplate getTemplate() {
    return template;
  }

  public int getMaxHealth() {
    return this.template.getHealth();
  }

  public int getMaxAttackPoints() {
    return activeAttackWeapon.getMaxPower();
  }

  public int getMinAttackPoints() {
    return this.activeAttackWeapon.getMinPower();
  }

  public int getMaxDefensePoints() {
    return this.activeDefenseWeapon.getMaxPower();
  }

  public int getMinDefensePoints() {
    return this.activeDefenseWeapon.getMinPower();
  }
}
