package com.anubhav.wrath.core.model;

/**
 * Status of a Game.
 */
public enum GameStatusEnum {

  IN_PROGRESS,
  COMPLETED

}
