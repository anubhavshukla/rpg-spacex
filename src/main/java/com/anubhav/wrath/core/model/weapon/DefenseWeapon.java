package com.anubhav.wrath.core.model.weapon;

/**
 * Defense weapon used by Player or Enemy.
 */
public class DefenseWeapon extends Weapon {

  /**
   * Template on which this weapon is based.
   */
  private DefenseWeaponTemplate template;

  public DefenseWeapon() {
  }

  public DefenseWeapon(DefenseWeaponTemplate template) {
    this.template = template;
  }

  public DefenseWeaponTemplate getTemplate() {
    return template;
  }

  public void setTemplate(DefenseWeaponTemplate template) {
    this.template = template;
  }

  public String getTemplateId() {
    return template.getId();
  }

  public String getName() {
    return template.getName();
  }

  public int getMaxPower() {
    return template.getMaxPower();
  }

  public int getMinPower() {
    return template.getMinPower();
  }
}
