package com.anubhav.wrath.core.model.character;

import com.anubhav.wrath.core.model.AbstractDataObject;

/**
 * Any character in the Game like Player, Enemy etc.
 */
public abstract class Character extends AbstractDataObject {

  private String name;
  private int health;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }
}
