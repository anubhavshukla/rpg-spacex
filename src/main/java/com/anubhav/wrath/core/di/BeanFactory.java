package com.anubhav.wrath.core.di;

import com.anubhav.wrath.core.calculator.DamageCalculator;
import com.anubhav.wrath.core.calculator.DamagePointCalculator;
import com.anubhav.wrath.core.calculator.ExperiencePointCalculator;
import com.anubhav.wrath.core.calculator.ExperiencePointCalculatorImpl;
import com.anubhav.wrath.core.exception.DIException;
import com.anubhav.wrath.core.repository.AttackWeaponTemplateRepository;
import com.anubhav.wrath.core.repository.DefenseWeaponTemplateRepository;
import com.anubhav.wrath.core.repository.EnemyTemplateRepository;
import com.anubhav.wrath.core.repository.GameSessionRepository;
import com.anubhav.wrath.core.repository.PlayerRepository;
import com.anubhav.wrath.core.repository.PlayerTemplateRepository;
import com.anubhav.wrath.core.repository.impl.AttackWeaponTemplateRepositoryImpl;
import com.anubhav.wrath.core.repository.impl.DefenseWeaponTemplateRepositoryImpl;
import com.anubhav.wrath.core.repository.impl.EnemyTemplateRepositoryImpl;
import com.anubhav.wrath.core.repository.impl.GameSessionRepositoryImpl;
import com.anubhav.wrath.core.repository.impl.PlayerRepositoryImpl;
import com.anubhav.wrath.core.repository.impl.PlayerTemplateRepositoryImpl;
import com.anubhav.wrath.core.service.FightService;
import com.anubhav.wrath.core.service.GameSessionService;
import com.anubhav.wrath.core.service.PlayerService;
import com.anubhav.wrath.core.service.impl.FightServiceImpl;
import com.anubhav.wrath.core.service.impl.GameSessionServiceImpl;
import com.anubhav.wrath.core.service.impl.PlayerServiceImpl;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of simple bean factory.
 *
 * TODO- Can be replaced by Spring IOC
 */
public class BeanFactory {

  private static final Map<Class, Object> beanStore = new HashMap<>();

  static {
    init();
  }

  private static void init() {
    addBean(AttackWeaponTemplateRepository.class, AttackWeaponTemplateRepositoryImpl.class);
    addBean(DefenseWeaponTemplateRepository.class, DefenseWeaponTemplateRepositoryImpl.class);
    addBean(EnemyTemplateRepository.class, EnemyTemplateRepositoryImpl.class);
    addBean(PlayerTemplateRepository.class, PlayerTemplateRepositoryImpl.class);
    addBean(PlayerRepository.class, PlayerRepositoryImpl.class);
    addBean(GameSessionRepository.class, GameSessionRepositoryImpl.class);

    addBean(DamageCalculator.class, DamagePointCalculator.class);
    addBean(ExperiencePointCalculator.class, ExperiencePointCalculatorImpl.class);

    beanStore.put(GameSessionService.class,
        new GameSessionServiceImpl(getBean(GameSessionRepository.class)));
    beanStore.put(PlayerService.class, new PlayerServiceImpl(
        getBean(PlayerTemplateRepository.class)));
    beanStore.put(FightService.class, new FightServiceImpl(getBean(DamageCalculator.class),
        getBean(ExperiencePointCalculator.class), getBean(EnemyTemplateRepository.class),
        getBean(GameSessionService.class)));
  }

  private static void addBean(Class className, Class implementationClass) {
    try {
      Object bean = implementationClass.newInstance();
      beanStore.put(className, bean);
    } catch (InstantiationException | IllegalAccessException e) {
      throw new DIException("Unable to initialize bean");
    }
  }

  @SuppressWarnings("unchecked")
  public static <T> T getBean(Class<T> clazz) {
    T bean;

    try {
      bean = (T) beanStore.get(clazz);
    } catch (Throwable t) {
      throw new DIException("Unable to initialize bean");
    }

    if (null == bean) {
      throw new DIException("Bean not found.");
    }

    return bean;
  }
}
