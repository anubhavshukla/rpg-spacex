package com.anubhav.wrath.core.i18n;

import com.anubhav.wrath.core.config.ApplicationProperties;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * Externalizing application messages. Read from file resources/messages/messages.properties. Can be
 * extended to provide multi-lingual support, not available currently.
 */
public class MessageResource {

  private static final String EMPTY_STRING = "";
  private static Properties prop = new Properties();

  static {
    InputStream is;
    try {
      prop = new Properties();
      is = ApplicationProperties.class.getResourceAsStream("/messages/message.properties");
      prop.load(is);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private MessageResource() {
  }

  public static String getMessage(String key) {
    String value = prop.getProperty(key);
    if (value == null) {
      return EMPTY_STRING;
    }
    return value;
  }

  public static String getMessage(String key, String... args) {
    return MessageFormat.format(getMessage(key), (Object[]) args);
  }
}
