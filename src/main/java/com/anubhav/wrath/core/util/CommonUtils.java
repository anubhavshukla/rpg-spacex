package com.anubhav.wrath.core.util;

import java.util.UUID;

/**
 * Some commonly used utilities.
 */
public class CommonUtils {

  public static String generateId() {
    return UUID.randomUUID().toString();
  }
}
