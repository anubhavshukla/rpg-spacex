package com.anubhav.wrath.core.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Load configurable properties from resources/application.properties file.
 */
public class ApplicationProperties {

  private static Properties prop = new Properties();

  private ApplicationProperties() {
    //instantiation not allowed
  }

  static {
    InputStream is;
    try {
      prop = new Properties();
      is = ApplicationProperties.class.getResourceAsStream("/application.properties");
      prop.load(is);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String get(String key) {
    return prop.getProperty(key);
  }
}
