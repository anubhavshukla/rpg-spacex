package com.anubhav.wrath.core.config;

/**
 * Keys used in application.properties file.
 */
public interface ApplicationPropertyKey {

  String PLAYER_TEMPLATE_ID = "default.player.template.id";
  String DEFAULT_COLUMN_WIDTH = "default.column.width";
}
